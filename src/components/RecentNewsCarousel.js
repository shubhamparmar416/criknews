import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";
import LeftImgCard from "./shared/LeftImgCard";
import RecentNewsCard from "./shared/RecentNewsCard";

import { NEWS_API_KEY, NEWS_URL, SPORT_SECRET, API_URL } from "../constant";
import axios from "axios";

const RecentNewsCarousel = () => {
  const [newsList, setNewsList] = useState(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      //const newsUrl = `${NEWS_URL}everything?apiKey=${NEWS_API_KEY}&q=Apple&pageSize=3&sortBy=publishedAt`;
      const newsUrl = `${NEWS_URL}top-headlines?country=in&category=sports&apiKey=${NEWS_API_KEY}&pageSize=3&sortBy=publishedAt`;
      const url = `${API_URL}callNewsApi`;
      const response = await axios.post(url, { url: newsUrl });
      const result = response.data.articles;
      console.log(result);
      setNewsList(result);
    } catch (err) {
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Container>
        <Row className="d-flex justify-content-between">
          <Col>
            <h4 className="text-black ps-3">Recent News</h4>{" "}
          </Col>
          {/* <Col>
            <h5 className="text-black text-end ps-4">Show All</h5>{" "}

          </Col> */}
        </Row>
      </Container>
      <Container>
        <Row>
          <Col sm={9}>
            <Carousel className="py-3 px-0 side-homes" nextLabel={"NEXT"}>
              {!loading && newsList.map((news, index) => {
                return (
                  <Carousel.Item>
                    <Container>
                      <Row>
                        <Col>
                          <RecentNewsCard news={news} />
                        </Col>
                      </Row>
                    </Container>
                  </Carousel.Item>
                );
              })}
            </Carousel>
          </Col>
          <Col sm={3} >
            <Row className="mt-3 bg-white rounded-4 py-1">
              {!loading && newsList.map((news, index) => {
                return (
                  <LeftImgCard news={news} />
                );
              })}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default RecentNewsCarousel;
