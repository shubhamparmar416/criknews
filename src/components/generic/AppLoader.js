import { Player, Controls } from "@lottiefiles/react-lottie-player";
function AppLoader({ height = "300px", width = "100%" }) {
  return (
    <Player
      autoplay
      loop
      src="../../assets/loader/cricket-bat-and-ball-bowled-out.json"
      style={{ height: height, width: width }}
    >
      <Controls
        visible={false}
        buttons={["play", "repeat", "frame", "debug"]}
      />
    </Player>
  );
}

export default AppLoader;
