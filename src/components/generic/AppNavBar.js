import { useState } from "react";
import { Button, Col, Row, Form, Collapse } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";

function AppNavBar() {
  // handleClick = () => {
  const [isVisible, initHs] = useState(false)
  const invokeCollapse = () => {
    return initHs(!isVisible)
  }
  // }
  return (
    <>
      <Navbar
        bg=""
        expand="lg"

        variant="dark"
        className="py-1 border-bottom"
      >
        <Container>
          <Navbar.Brand href="/">
            <img src="../assets/img/ibetin.png" alt="Logo" />
          </Navbar.Brand>
          <div className="d-flex jst-cnt">
            <Form className="me-3 d-flex">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2 h-serch  bton-none"
                aria-label="Search"
              />
              {/* <Button variant="outline-success">Search</Button> */}

              <div class="input-group-append  bton-none">
                <button class="btn btn-outline-secondary  h-serch" type="button">
                  <img src="../assets/img/loupe.png" />
                </button>
              </div>
              <div className="mob-serch">
                <Button className=" btn btn-outline-secondary  h-serch" onClick={invokeCollapse}>
                  <img src="../assets/img/loupe.png" width={"20px"} />

                </Button>

              </div>
            </Form>
            <Nav.Link className=" bton-none" as={Link} to="/signup">
              <Button className="btn btn-base b-animate-4">Sign Up</Button>
            </Nav.Link>

            <Nav.Link className=" bton-none" as={Link} to="/login">
              <Button className="btn btn-base b-animate-4">Login</Button>
            </Nav.Link>
          </div>
        </Container>

      </Navbar>
      <Container>
        <Collapse in={isVisible}>
          <div id="collapsePanel">
            <div className="d-flex jst-cnt">

              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2 mt-2 h-serch  "
                aria-label="Search"
              />
              <div class="input-group-append  mt-2">
                <button class="btn btn-outline-secondary  h-serch" type="button">
                  <img src="../assets/img/loupe.png" width={"20px"} />
                </button>
              </div>
            </div>
          </div>
        </Collapse></Container>
      {/* <Container>
        <Row>
          <Col>

          </Col>
          <Navbar
            className="bg-black rounded-3">
            <Navbar.Toggle aria-controls="navbarScroll" />

            <Navbar.Collapse id="navbarScroll" className="justify-content-end ">
              <Nav
                className="me-auto my-2 my-lg-0 justify-content-end align-items-center navbar-dark  "
                style={{ maxHeight: "100px" }}
                navbarScroll
                activeKey="/"
              >
                <Nav.Link className="text-white" as={Link} to="/">
                  All
                </Nav.Link>


              </Nav>

            </Navbar.Collapse>
          </Navbar>
        </Row>
      </Container> */}
    </>
  );
}

export default AppNavBar;
