import React from "react";

import Carousel from "react-bootstrap/Carousel";

const CustomCarousel = ({ nextLabel, children }) => {
  return (
    <Carousel className="py-9 border-bottom-muted" nextLabel={nextLabel}>
      {children}
    </Carousel>
  );
};

export default CustomCarousel;
