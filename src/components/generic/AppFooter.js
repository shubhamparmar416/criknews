import React from "react";
import { Container, Row, Col, Nav, Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";
const AppFooter = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className="foot d-none d-md-block">
        <div className="footer-area bg-red  ">

          <Row className="border-bottom-muted justify-content-center">
            <Col sm="9">
              <Row>
                <Col sm={4}>
                  {" "}
                  <div className="widget widget_about">
                    <div className="logo mb-3">
                      <a href="index.html">
                        <img src="../assets/img/ibetin.png" alt="Logo" />
                      </a>
                    </div>
                    <div className="details">
                      <p className="mb-3">
                        {/* <FontAwesomeIcon icon={faCoffee} size="2x" />
                    <FontAwesomeIcon icon={faAirFreshener} /> */}
                        Follow Us
                      </p >
                      <a href="https://www.instagram.com/ibetinnews/" target="_blank">
                        <svg className="me-3"
                          width="25"
                          height="25"
                          viewBox="0 0 14 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M4.11409 0.042C4.86055 0.00763635 5.09855 0 7 0C8.90145 0 9.13945 0.00827272 9.88527 0.042C10.6311 0.0757273 11.1402 0.194727 11.5856 0.367182C12.0521 0.543455 12.4753 0.819 12.8253 1.17536C13.1816 1.52473 13.4565 1.94727 13.6322 2.41436C13.8053 2.85982 13.9236 3.36891 13.958 4.11345C13.9924 4.86118 14 5.09918 14 7C14 8.90145 13.9917 9.13945 13.958 9.88591C13.9243 10.6305 13.8053 11.1395 13.6322 11.585C13.4565 12.0522 13.1812 12.4754 12.8253 12.8253C12.4753 13.1816 12.0521 13.4565 11.5856 13.6322C11.1402 13.8053 10.6311 13.9236 9.88655 13.958C9.13945 13.9924 8.90145 14 7 14C5.09855 14 4.86055 13.9917 4.11409 13.958C3.36955 13.9243 2.86045 13.8053 2.415 13.6322C1.94786 13.4565 1.52461 13.1812 1.17473 12.8253C0.818605 12.4757 0.543018 12.0527 0.367182 11.5856C0.194727 11.1402 0.0763636 10.6311 0.042 9.88655C0.00763635 9.13882 0 8.90082 0 7C0 5.09855 0.00827272 4.86055 0.042 4.11473C0.0757273 3.36891 0.194727 2.85982 0.367182 2.41436C0.543278 1.94732 0.819076 1.52429 1.17536 1.17473C1.52475 0.818683 1.94757 0.5431 2.41436 0.367182C2.85982 0.194727 3.36891 0.0763636 4.11345 0.042H4.11409ZM9.82864 1.302C9.09045 1.26827 8.869 1.26127 7 1.26127C5.131 1.26127 4.90955 1.26827 4.17136 1.302C3.48855 1.33318 3.11818 1.44709 2.87127 1.54318C2.54482 1.67045 2.31127 1.82127 2.06627 2.06627C1.83403 2.29221 1.6553 2.56727 1.54318 2.87127C1.44709 3.11818 1.33318 3.48855 1.302 4.17136C1.26827 4.90955 1.26127 5.131 1.26127 7C1.26127 8.869 1.26827 9.09045 1.302 9.82864C1.33318 10.5115 1.44709 10.8818 1.54318 11.1287C1.65518 11.4323 1.834 11.7078 2.06627 11.9337C2.29218 12.166 2.56773 12.3448 2.87127 12.4568C3.11818 12.5529 3.48855 12.6668 4.17136 12.698C4.90955 12.7317 5.13036 12.7387 7 12.7387C8.86964 12.7387 9.09045 12.7317 9.82864 12.698C10.5115 12.6668 10.8818 12.5529 11.1287 12.4568C11.4552 12.3295 11.6887 12.1787 11.9337 11.9337C12.166 11.7078 12.3448 11.4323 12.4568 11.1287C12.5529 10.8818 12.6668 10.5115 12.698 9.82864C12.7317 9.09045 12.7387 8.869 12.7387 7C12.7387 5.131 12.7317 4.90955 12.698 4.17136C12.6668 3.48855 12.5529 3.11818 12.4568 2.87127C12.3295 2.54482 12.1787 2.31127 11.9337 2.06627C11.7078 1.83405 11.4327 1.65532 11.1287 1.54318C10.8818 1.44709 10.5115 1.33318 9.82864 1.302ZM6.10591 9.15791C6.60524 9.36576 7.16124 9.39382 7.67895 9.23728C8.19667 9.08074 8.64398 8.74931 8.94448 8.29961C9.24498 7.84991 9.38004 7.30983 9.32658 6.77161C9.27312 6.2334 9.03446 5.73044 8.65136 5.34864C8.40715 5.10458 8.11186 4.9177 7.78676 4.80146C7.46165 4.68522 7.11482 4.6425 6.77122 4.67639C6.42763 4.71028 6.09582 4.81993 5.79969 4.99745C5.50356 5.17496 5.25047 5.41593 5.05865 5.703C4.86682 5.99006 4.74103 6.31609 4.69033 6.65761C4.63963 6.99913 4.66528 7.34764 4.76543 7.67806C4.86559 8.00847 5.03776 8.31257 5.26955 8.56846C5.50134 8.82435 5.78698 9.02566 6.10591 9.15791ZM4.45582 4.45582C4.78992 4.12171 5.18657 3.85668 5.6231 3.67587C6.05963 3.49505 6.5275 3.40198 7 3.40198C7.4725 3.40198 7.94037 3.49505 8.3769 3.67587C8.81343 3.85668 9.21008 4.12171 9.54418 4.45582C9.87829 4.78992 10.1433 5.18657 10.3241 5.6231C10.505 6.05963 10.598 6.5275 10.598 7C10.598 7.4725 10.505 7.94037 10.3241 8.3769C10.1433 8.81343 9.87829 9.21008 9.54418 9.54418C8.86942 10.2189 7.95425 10.598 7 10.598C6.04575 10.598 5.13058 10.2189 4.45582 9.54418C3.78106 8.86942 3.40198 7.95425 3.40198 7C3.40198 6.04575 3.78106 5.13058 4.45582 4.45582ZM11.396 3.93782C11.4788 3.85972 11.5451 3.7658 11.5909 3.66162C11.6368 3.55744 11.6612 3.44513 11.6629 3.33132C11.6646 3.21751 11.6434 3.10453 11.6006 2.99906C11.5578 2.89359 11.4943 2.79778 11.4138 2.7173C11.3333 2.63682 11.2375 2.5733 11.132 2.53051C11.0266 2.48772 10.9136 2.46653 10.7998 2.46819C10.686 2.46985 10.5736 2.49433 10.4695 2.54017C10.3653 2.58602 10.2714 2.6523 10.1933 2.73509C10.0414 2.89611 9.95822 3.10999 9.96145 3.33132C9.96467 3.55265 10.054 3.76401 10.2106 3.92054C10.3671 4.07706 10.5784 4.16642 10.7998 4.16964C11.0211 4.17287 11.235 4.08971 11.396 3.93782Z"
                            fill="white"
                          />
                        </svg></a>
                      <a href="https://www.facebook.com/ibetinnews/" target="_blank">

                        <svg
                          width="25"
                          height="25"
                          viewBox="0 0 7 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M1.73704 14V7.43079H0V5.06556H1.73704V3.04535C1.73704 1.45785 2.76311 0 5.1274 0C6.08467 0 6.79252 0.09177 6.79252 0.09177L6.73674 2.30049C6.73674 2.30049 6.01485 2.29347 5.22708 2.29347C4.37448 2.29347 4.23788 2.68638 4.23788 3.33851V5.06556H6.80452L6.69284 7.43079H4.23788V14H1.73704Z"
                            fill="white"
                          />
                        </svg>
                      </a>
                    </div>
                  </div>
                </Col>
                {/* <Col sm={3}>
                  <div className="widget widget_nav_menu">

                    <Nav className="d-block">
                      <Nav.Link className="text-light" href="#">

                        About Us
                      </Nav.Link>

                      <Nav.Link className="text-light" href="#">
                        Affiliate Writing
                      </Nav.Link>

                    </Nav>
                  </div>
                </Col> */}
                <Col sm={4}>
                  <div className="widget widget_nav_menu">

                    <Nav className="d-block">
                      <Nav.Link className="text-light" to="/about" as={Link}
                      >

                        About Us
                      </Nav.Link>

                      {/* <Nav.Link className="text-light" href="#">
                        Affiliate Writing
                      </Nav.Link> */}

                      <Nav.Link
                        className="text-light"
                        to="/privacy" as={Link}
                      >
                        Privacy Policy
                      </Nav.Link>
                      <Nav.Link
                        className="text-light"
                        to="/terms" as={Link}
                      >
                        Terms & Conditions
                      </Nav.Link>
                    </Nav>
                  </div>
                </Col>
                <Col sm={4}>
                  <div className="widget widget_nav_menu">
                    <h6 className="text-white">Subscribe to Newsletter</h6>
                    <Form className="d-flex">
                      <Form.Group className="mb-3" controlId="formBasicEmail">

                        <Form.Control className="rounded-4 p-1" type="email" />

                      </Form.Group>

                      <Button className="news-btn" type="submit">
                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" class="text-black" height="17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z"></path></svg>
                      </Button>
                    </Form>
                  </div>
                </Col>
              </Row>
            </Col>

          </Row>
        </div>
        <div className="footer-area-bottom bg-red py-3 d-flex align-items-center">
          <Container>
            <Row className="align-items-center pb-4">
              <Col sm={12}>
                <p className="text-light text-center p-0 m-0 ft-size-11">
                  Address:- Nearby Gajanan High school, Ground, Shop No. 6, Shri Sadguru Krupa, High
                  Tension Road, Nallasopara East, Vasai Virar, Palghar, Maharashtra, 401209
                </p>
              </Col>

            </Row>
          </Container>
        </div>

      </div>
      <div className=" mobile-footer">
        <ul className="justify-content-evenly d-flex list-unstyled mb-0">
          <li>
            <a href="#" className="d-flex flex-column justify-content-center text-decoration-none align-items-center" onClick={() => navigate("/")}>
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 9.84381C0 10.504 0.0619845 10.8228 0.556254 11.3188C0.833814 11.5974 1.24144 11.848 1.79731 11.8356C2.29116 11.8245 2.14848 11.7924 2.14848 13.2422C2.14848 14.3073 2.07067 17.9093 2.21769 18.4465C2.44697 19.2842 3.29637 20.0001 4.1797 20.0001H7.50002C8.24686 20.0001 8.08599 19.0904 8.08599 18.672L8.08505 14.8429C8.0816 14.2058 8.47061 13.7501 8.94537 13.7501H11.0547C11.3262 13.7501 11.5339 13.9227 11.6715 14.0708C11.8345 14.2465 11.9159 14.4712 11.9155 14.8034C11.9149 15.3638 11.8658 19.3431 11.9362 19.5873C12.0027 19.8179 12.218 20.0001 12.5001 20.0001H15.8204C16.3253 20.0001 16.8037 19.7494 17.0806 19.5025C18.0041 18.679 17.8516 18.0363 17.8516 16.7579V12.3829C17.8508 11.8873 17.7255 11.8249 18.2028 11.8356C19.2231 11.8584 19.943 11.0009 20.0001 10.0896V9.85652C19.9812 9.55964 19.8894 9.26445 19.7118 8.99927C19.4572 8.6191 12.6601 1.89842 11.709 0.947294C11.0098 0.247985 10.863 0 9.84381 0C9.13349 0 8.68085 0.557529 8.31059 0.927735L0.927735 8.31059C0.557529 8.68085 0 9.13349 0 9.84381Z" fill="white" />
              </svg>
              <p className="text-white mb-0 fw-bold">Home</p>
            </a>
          </li>
          <li>
            <a href="#" className="d-flex flex-column justify-content-center text-decoration-none align-items-center" onClick={() => navigate("/login")}>
              <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.64065 1.44539V5.00009H1.01561C0.436915 5.00009 0 5.32986 0 5.8204V10.8985C0 11.3845 0.437434 11.7188 1.01561 11.7188H6.64065V15.2735C6.64065 16.1596 7.45684 16.6721 8.1544 15.9669L15.0879 9.03329C15.8789 8.24762 15.1757 7.77352 14.6191 7.21689L8.17387 0.771451C7.42131 0.013937 6.64065 0.596081 6.64065 1.44539ZM11.6406 0.703136L11.6797 1.52353C11.8105 1.59275 11.8799 1.65487 12.0573 1.69271C12.176 1.71799 12.3991 1.71927 12.5781 1.71884L15.8984 1.71875C16.3419 1.71875 16.5478 1.69219 16.9004 1.81054C17.2243 1.91939 17.3579 2.01544 17.6014 2.20333C17.7679 2.33183 17.99 2.62607 18.0867 2.81179C18.2087 3.04603 18.3203 3.35533 18.3203 3.71099V13.0079C18.3203 13.5636 18.1073 13.9174 17.8296 14.2749C17.7082 14.4313 17.6866 14.4512 17.5339 14.5653C16.9136 15.0288 16.6526 15.0001 15.625 15.0001L12.1131 15.0038C11.5629 15.0591 11.6406 15.5621 11.6406 16.0938C11.6406 16.5375 11.8219 16.7189 12.2657 16.7189C13.356 16.7189 15.7663 16.7712 16.7524 16.6743C17.365 16.6141 17.9902 16.3318 18.4714 15.9715C18.9391 15.6212 19.3142 15.1966 19.5966 14.6749C19.8065 14.2869 20 13.7062 20 13.1251V3.5547C20 2.81495 19.6499 1.99876 19.2246 1.4786C18.9176 1.10311 18.6251 0.835987 18.2195 0.569671C17.4985 0.0961892 16.7134 0 15.8594 0H12.2266C11.7504 0 11.6406 0.231827 11.6406 0.703136Z" fill="white" />
              </svg>

              <p className="text-white mb-0 fw-bold">Sign in</p>
            </a>
          </li>
          <li>
            <a href="#" className="d-flex flex-column justify-content-center text-decoration-none align-items-center" onClick={() => navigate("/privacy")}>
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.8984 13.9843V11.7578C5.20811 11.7578 4.70368 11.7589 4.12971 12.3328C3.58877 12.8737 3.5547 13.3482 3.5547 14.0235C3.5547 15.285 4.01841 16.5492 4.45258 17.3833C4.68894 17.8374 6.09304 19.8511 6.13282 20H11.5625C11.5994 19.8417 11.7443 19.5988 11.8233 19.4406C12.4512 18.1859 12.9297 16.1891 12.9297 14.7657C12.9297 13.8585 13.0951 12.958 12.4123 12.2752C12.2504 12.1134 12.1656 12.0311 11.9408 11.9263C11.7307 11.8284 11.5421 11.7578 11.25 11.7578H9.41403V8.12502C9.41403 7.62154 8.86283 7.07034 8.35935 7.07034C7.89465 7.07034 7.62626 7.125 7.29782 7.53224C7.04648 7.84387 7.07024 8.09615 7.07034 8.63284L7.07166 13.9076C7.07293 14.9803 5.8984 14.8931 5.8984 13.9843ZM0 4.49222C0 5.57175 0.112772 6.20922 0.65627 7.11716C0.729876 7.24013 0.784585 7.33623 0.869955 7.45037C1.699 8.55844 3.07532 9.41403 4.49222 9.41403C5.19786 9.41403 5.20306 9.41984 5.8984 9.25779C5.8984 8.13177 5.86419 7.53961 6.36092 6.86875C6.89638 6.14544 7.82091 5.74844 8.75955 5.96699C8.93573 6.008 9.04491 6.07929 9.21872 6.0937C9.22505 5.81156 9.41403 5.64465 9.41403 4.49222C9.41403 3.87242 9.18499 3.13352 8.97182 2.70785C8.2118 1.1898 6.67788 0 4.96093 0C3.68926 0 3.00138 0.187418 1.99947 0.866696C1.89615 0.936711 1.77851 1.01915 1.68893 1.10301C1.17411 1.5849 0.778302 2.00368 0.455057 2.68163C0.240709 3.13116 0 3.85163 0 4.49222H0ZM2.7395 3.98165L3.90398 5.14608L6.67457 2.36576L7.51632 3.20278L3.90308 6.82868L1.89775 4.8234L2.7395 3.98165ZM10.586 4.49222C10.586 5.44102 10.6358 5.89126 11.0284 6.74506C11.7004 8.20632 13.3964 9.41403 15.0391 9.41403H15.5078C17.2552 9.41403 18.8076 8.20151 19.5313 6.75782C19.7959 6.23 19.9607 5.6001 20 4.96551V4.38209C19.9809 4.08233 19.9313 3.78833 19.8486 3.51077C19.6531 2.85445 19.5046 2.54783 19.1336 1.99923C18.9235 1.6887 18.3346 1.09791 18.0242 0.882097C17.2525 0.34545 16.4018 0.0540474 15.5519 0H14.97C13.8696 0.0707246 12.8019 0.541466 11.9433 1.39645C11.3103 2.02682 10.586 3.24847 10.586 4.49222ZM12.8936 6.18489L14.4111 4.6537L12.8935 3.12247L13.7818 2.23909L15.293 3.76385L16.8041 2.23909L17.6924 3.12247L16.1749 4.6537L17.6924 6.18489L16.804 7.06831L15.293 5.54359L13.7819 7.06831L12.8936 6.18489Z" fill="white" />
              </svg>

              <p className="text-white mb-0 fw-bold">Privacy</p>
            </a>
          </li>
          <li>
            <a href="#" className="d-flex flex-column justify-content-center text-decoration-none align-items-center" onClick={() => navigate("/terms")}>
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.8984 13.9843V11.7578C5.20811 11.7578 4.70368 11.7589 4.12971 12.3328C3.58877 12.8737 3.5547 13.3482 3.5547 14.0235C3.5547 15.285 4.01841 16.5492 4.45258 17.3833C4.68894 17.8374 6.09304 19.8511 6.13282 20H11.5625C11.5994 19.8417 11.7443 19.5988 11.8233 19.4406C12.4512 18.1859 12.9297 16.1891 12.9297 14.7657C12.9297 13.8585 13.0951 12.958 12.4123 12.2752C12.2504 12.1134 12.1656 12.0311 11.9408 11.9263C11.7307 11.8284 11.5421 11.7578 11.25 11.7578H9.41403V8.12502C9.41403 7.62154 8.86283 7.07034 8.35935 7.07034C7.89465 7.07034 7.62626 7.125 7.29782 7.53224C7.04648 7.84387 7.07024 8.09615 7.07034 8.63284L7.07166 13.9076C7.07293 14.9803 5.8984 14.8931 5.8984 13.9843ZM0 4.49222C0 5.57175 0.112772 6.20922 0.65627 7.11716C0.729876 7.24013 0.784585 7.33623 0.869955 7.45037C1.699 8.55844 3.07532 9.41403 4.49222 9.41403C5.19786 9.41403 5.20306 9.41984 5.8984 9.25779C5.8984 8.13177 5.86419 7.53961 6.36092 6.86875C6.89638 6.14544 7.82091 5.74844 8.75955 5.96699C8.93573 6.008 9.04491 6.07929 9.21872 6.0937C9.22505 5.81156 9.41403 5.64465 9.41403 4.49222C9.41403 3.87242 9.18499 3.13352 8.97182 2.70785C8.2118 1.1898 6.67788 0 4.96093 0C3.68926 0 3.00138 0.187418 1.99947 0.866696C1.89615 0.936711 1.77851 1.01915 1.68893 1.10301C1.17411 1.5849 0.778302 2.00368 0.455057 2.68163C0.240709 3.13116 0 3.85163 0 4.49222H0ZM2.7395 3.98165L3.90398 5.14608L6.67457 2.36576L7.51632 3.20278L3.90308 6.82868L1.89775 4.8234L2.7395 3.98165ZM10.586 4.49222C10.586 5.44102 10.6358 5.89126 11.0284 6.74506C11.7004 8.20632 13.3964 9.41403 15.0391 9.41403H15.5078C17.2552 9.41403 18.8076 8.20151 19.5313 6.75782C19.7959 6.23 19.9607 5.6001 20 4.96551V4.38209C19.9809 4.08233 19.9313 3.78833 19.8486 3.51077C19.6531 2.85445 19.5046 2.54783 19.1336 1.99923C18.9235 1.6887 18.3346 1.09791 18.0242 0.882097C17.2525 0.34545 16.4018 0.0540474 15.5519 0H14.97C13.8696 0.0707246 12.8019 0.541466 11.9433 1.39645C11.3103 2.02682 10.586 3.24847 10.586 4.49222ZM12.8936 6.18489L14.4111 4.6537L12.8935 3.12247L13.7818 2.23909L15.293 3.76385L16.8041 2.23909L17.6924 3.12247L16.1749 4.6537L17.6924 6.18489L16.804 7.06831L15.293 5.54359L13.7819 7.06831L12.8936 6.18489Z" fill="white" />
              </svg>

              <p className="text-white mb-0 fw-bold">Terms</p>
            </a>
          </li>
        </ul>
      </div>
      <div className="whtsp">
        <a href="https://api.whatsapp.com/send/?phone=918349109565&text&type=phone_number&app_absent=0" target="_blank">
          <img src="../assets/img/whatsapp.svg" width={"40px"} />
        </a>
      </div>
    </>
  );
};

export default AppFooter;
