import React, { Suspense, lazy } from "react";
import { Routes, Route } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import Contact from "../../pages/Contact";
import Terms from "../../pages/Terms";
import NewsDetails from "../../pages/News-Details";
import Login from "../../pages/Login";
import SignUp from "../../pages/SignUp";
import Privacy from "../../pages/Privacy";
import AboutUs from "../../pages/AboutUs";

const Home = lazy(() => import("../../pages/Home"));

const AppContent = () => (
  <Suspense fallback={<Spinner animation="border" variant="primary" />}>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/contact" element={<Contact />} />
      <Route path="/about" element={<AboutUs />} />
      <Route path="/terms" element={<Terms />} />
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<SignUp />} />
      <Route path="/privacy" element={<Privacy />} />
      <Route path="/news-details" element={<NewsDetails />} />
    </Routes>
  </Suspense>
);

export default AppContent;
