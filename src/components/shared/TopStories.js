import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";
import TopStoriesCard from "./TopStoriesCard";
import { isCompositeComponent } from "react-dom/test-utils";
import { NEWS_API_KEY, NEWS_URL, SPORT_SECRET, API_URL } from "../../constant";

import axios from "axios";
const TopStories = () => {
	const [topNewsList, setTopNewsList] = useState(null);
	const [loading, setLoading] = useState(true);
	useEffect(() => {
		getData();
	}, []);
	const getData = async () => {
		try {
			const date = new Date();
			date.setDate(date.getDate() - 1)
			const yesterday = date.toLocaleDateString('en-US');
			const newsUrl = `${NEWS_URL}top-headlines?country=in&category=sports&pageSize=3&apiKey=${NEWS_API_KEY}&sortBy=popularity`;
			//const newsUrl = `${NEWS_URL}everything?apiKey=${NEWS_API_KEY}&q=Apple&pageSize=3&sortBy=popularity&from=`+yesterday;
			const url = `${API_URL}callNewsApi`;
			const response = await axios.post(url, { url: newsUrl });
			const result = response.data.articles;
			console.log(result);
			setTopNewsList(result);
		} catch (err) {
		} finally {
			setLoading(false);
		}
	};


	return (
		<>
			<Container>
				<Row className="d-flex justify-content-between">
					<Col>
						<h4 className="text-black mb-3 ps-3">Top Stories</h4>{" "}
					</Col>
					{/* <Col>
						<h5 className="text-black mb-3 ps-4 text-end">Show All</h5>{" "}

					</Col> */}
				</Row>
				<Row className="">
					{!loading &&
						topNewsList.map((news, index) => {
							return (
								<Col sm={4} className="mb-2">
									<TopStoriesCard news={news} />
								</Col>
							);
						})}

				</Row>
			</Container>
		</>
	);
};

export default TopStories;
