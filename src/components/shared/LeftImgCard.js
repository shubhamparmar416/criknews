import React from "react";
import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router-dom";
import { calculateDays,shareLink } from "../../helpers/utils";

import { PLACEHOLDER_THUMBNAIL } from "../../constant";
import { Col, Row } from "react-bootstrap";
const LeftImgCard = ({ news }) => {
  const navigate = useNavigate();
  const handleReadMore = (news) => {
    localStorage.setItem("newsItem", JSON.stringify(news));
    navigate("/news-details");
  };
  return (
    <>
      <Card className=" mt-1 rounded-4 featured-card border-0">
        <Row className="thumb tops-area-inner d-flex align-items-center">
          <Col sm={5} className="">
            <span className="shadow"></span>
            <Card.Img
              className="w-100 left-crd"
              src={(news.urlToImage) ? news.urlToImage : `../assets/img/${PLACEHOLDER_THUMBNAIL}`}

            />
          </Col>
          <Col sm={7} className="">

            <Card.Body className="details ">

              <Card.Text className="ft-size-14 fw-500 pt-0 text-black text-clamps-1 cursur-pointer mb-1"
                onClick={() => handleReadMore(news)} >
                {news?.title}
              </Card.Text>
              <div className="details">
                {/* <h1>dfdsfdsfdsfd</h1> */}
                <p className="ft-size-12 text-black text-clamps m-0">{news?.description} </p>
              </div>
              <ul>
                <li>
                  <div class="d-flex align-items-center">
                    {/* <p class="ft-size-12 pe-2">ICC World Test Championship</p> */}
                    <p class="ft-size-12 m-0"><span class="ft-size-6 pe-1"></span>{calculateDays(news.publishedAt)} </p>

                    <p class="ft-size-12 pe-2 m-0"><span class="ft-size-6 pe-1"> ●</span>Cricket</p>


                  </div>
                </li>
                <li onClick={() => shareLink()}>
                  <div class="flex items-end justify-center color-gray w-10 h-10" aria-expanded="false" aria-haspopup="dialog">
                    <span class="text-[6px]">●</span><span class="text-[6px]">●</span><span class="text-[6px]">●</span></div>
                </li>
              </ul>
            </Card.Body>
          </Col>
        </Row>


      </Card>
    </>
  );
};

export default LeftImgCard;
