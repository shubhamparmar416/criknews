import React, { useEffect, useState } from "react";
// import Button from "react-bootstrap/Button";
// import Card from "react-bootstrap/Card";
// import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";
import LeftImgCard from "./LeftImgCard";

import { NEWS_API_KEY, NEWS_URL, SPORT_SECRET, API_URL } from "../../constant";
import axios from "axios";

import { listToGroupList } from "../../helpers/utils";

const BottomStories = () => {
  const [newsList, setNewsList] = useState(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      //const newsUrl = `${NEWS_URL}everything?apiKey=${NEWS_API_KEY}&q=Apple&pageSize=9&sortBy=publishedAt`;
      const newsUrl = `${NEWS_URL}top-headlines?country=in&category=sports&pageSize=9&apiKey=${NEWS_API_KEY}&sortBy=publishedAt`;
      const url = `${API_URL}callNewsApi`;
      const response = await axios.post(url, { url: newsUrl });
      const result = listToGroupList(response.data.articles, 3);
      console.log(result);
      setNewsList(result);
    } catch (err) {
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <Container className="mob-m">
        {newsList?.map((news, index) => {
          return (
            <Row className="">
              {news.map((items, i) => {
                return (
                  <Col sm={4} className="bot-cards">
                    <LeftImgCard news={items} />
                  </Col>
                );
              })}
            </Row>
          )
        })}

      </Container>
    </>
  );
};

export default BottomStories;
