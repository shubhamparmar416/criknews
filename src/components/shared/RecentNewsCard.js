import React from "react";
import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router-dom";
import { calculateDays,shareLink } from "../../helpers/utils";
import { PLACEHOLDER_THUMBNAIL } from "../../constant";
const RecentNewsCard = ({ news }) => {
  const navigate = useNavigate();
  const handleReadMore = (news) => {
    localStorage.setItem("newsItem", JSON.stringify(news));
    navigate("/news-details");
  };
  
  return (
    <>
      <Card className="bg-white border-0 rounded-lg-5">
        <Card.Body className="p-0">
          <div className="single-intro-inner">
            <Card.Img
              className="rounded-4 p-0"
              variant="top"
              src={(news.urlToImage) ? news.urlToImage : `../assets/img/${PLACEHOLDER_THUMBNAIL}`}

              alt="Recent news"
              onError={(e) => {
                e.target.src = `../assets/img/${PLACEHOLDER_THUMBNAIL}`;
              }}
            />

            <div className="details p-3">
              <h1 className="cursur-pointer text-clamps"
                onClick={() => handleReadMore(news)}
              >{news.title}</h1>
              <p className="text-clamps">{news?.description}</p>
            </div>
            <ul>
              <li>
                <div class="d-flex align-items-center">
                  {/* <p class="ft-size-12 pe-2">ICC World Test Championship</p> */}
                  <p class="ft-size-12 pe-2"><span class="ft-size-6 pe-1">●</span>Cricket</p>
                  <p class="ft-size-12 ">
                    <span class="ft-size-6 pe-1">●</span>
                    {calculateDays(news.publishedAt)} ago</p></div>
              </li>
              <li onClick={() => shareLink()}>
                <div class="flex items-end justify-center color-gray w-10 h-10" aria-expanded="false" aria-haspopup="dialog">
                  <span class="text-[6px]">●</span><span class="text-[6px]">●</span><span class="text-[6px]">●</span></div>
              </li>
            </ul>
          </div>{" "}
        </Card.Body>
      </Card>
    </>
  );
};

export default RecentNewsCard;
