import React from "react";
import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router-dom";
import { calculateDays,shareLink } from "../../helpers/utils";

import { PLACEHOLDER_THUMBNAIL } from "../../constant";
const TrendingCard = ({ trendingNews }) => {
  const navigate = useNavigate();
  const handleReadMore = (news) => {
    localStorage.setItem("newsItem", JSON.stringify(news));
    navigate("/news-details");
  };
  return (
    <>
      <Card className=" mt-1 rounded-4 featured-card border-0">
        <div className="thumb video-area-inner">
          <span className="shadow"></span>
          <Card.Img
            className="rounded-4 p-2"
            variant="top"
            src={(trendingNews.urlToImage) ? trendingNews.urlToImage : `../assets/img/${PLACEHOLDER_THUMBNAIL}`}

          />
        </div>

        <Card.Body className="details pt-0">

          <Card.Text className="ft-size-14 fw-500 pt-0 text-black text-clamps cursur-pointer"
            onClick={() => handleReadMore(trendingNews)}>
            {trendingNews.title}
          </Card.Text>
          <ul>
            <li>
              <div class="d-flex align-items-center">
                {/* <p class="ft-size-11 pe-2">ICC World Test Championship</p> */}
                <p class="ft-size-11 m-0">{calculateDays(trendingNews.publishedAt)}</p>
                <p class="ft-size-11 pe-2 m-0"><span class="ft-size-6 pe-1">●</span>Cricket</p>

              </div>
            </li>
            <li onClick={() => shareLink()}>
              <div class="flex items-end justify-center color-gray w-10 h-10" aria-expanded="false" aria-haspopup="dialog">
                <span class="text-[6px]">●</span><span class="text-[6px]">●</span><span class="text-[6px]">●</span></div>
            </li>
          </ul>
        </Card.Body>
      </Card>
    </>
  );
};

export default TrendingCard;
