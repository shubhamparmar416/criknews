import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";
import TrendingCard from "./TrendingCard";

import { NEWS_API_KEY, NEWS_URL, API_URL } from "../../constant";
import { listToGroupList } from "../../helpers/utils";
import axios from "axios";

const Trending = () => {
    const [trendingNews, setTrendingNews] = useState(null);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        getData();
    }, []);
    const getData = async () => {
        try {
            const date = new Date();
            date.setDate(date.getDate() - 1)
            const yesterday = date.toLocaleDateString('en-US');
            //const newsUrl = `${NEWS_URL}everything?apiKey=${NEWS_API_KEY}&q=Apple&sortBy=popularity&from=`+yesterday;
            const newsUrl = `${NEWS_URL}top-headlines?country=in&category=sports&apiKey=${NEWS_API_KEY}&sortBy=popularity`;
            const url = `${API_URL}callNewsApi`;
            const response = await axios.post(url, { url: newsUrl });
            const result = listToGroupList(response.data.articles, 6);
            setTrendingNews(result);
        } catch (err) {
        } finally {
            setLoading(false);
        }
    };
    return (
        <>
            <Container>
                <Row className="d-flex justify-content-between">
                    <Col>
                        <h4 className="text-black ps-4">Trending</h4>{" "}
                    </Col>
                    {/* <Col>
                        <h5 className="text-black ps-4 text-end">Show All</h5>{" "}
                    </Col> */}
                </Row>
                <Row>
                    <Col sm="12">
                        <Carousel className="py-3 px-0 side-homes trendings">
                            {trendingNews?.map((news, index) => {
                                return (
                                    <Carousel.Item>
                                        <Container>
                                            <Row>
                                                {news.map((items, i) => {
                                                    return (
                                                        <Col>
                                                            <TrendingCard trendingNews={items} />
                                                        </Col>
                                                    )
                                                })}
                                            </Row>
                                        </Container>
                                    </Carousel.Item>
                                );
                            })}
                        </Carousel>
                    </Col>

                </Row>
            </Container>
        </>
    );
};

export default Trending;
