import React from "react";

import {
    Button,
    Card,
    Container,
    Row,
    Col,
    Breadcrumb,
    Badge,
} from "react-bootstrap";
const AboutUs = () => {
    return (

        <Container>
            <Row className="pt-5 mt-5 pb-4">
                <Col sm={6}>
                    <h3>About Us</h3>
                    <p>Ibetin news the destination for all the sports lovers. We cover the sports news around the world for our favourite sports such as Cricket, Football, Tennis. You can get the latest news from the world of  sports.<br></br>

                        Head towards the ibetin news to get the latest news about the world of sports from our expeorenced writers of sports desk.<br></br>

                        Join us for your sports cravings.
                    </p>
                </Col>


            </Row>
        </Container>
    )
};

export default AboutUs;
