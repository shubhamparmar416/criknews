import React from "react";
import { Container, Row, Col } from "react-bootstrap";
const Privacy = () => {
  return (
    <div className="fact-area-3 intro-area bg-navy-2 mt-0">
      {/* <div className="breadcrumb-inner text-center ">
        <h1 className="page-title text-white">Privacy Policy</h1>
        <Breadcrumb className="justify-content-center d-flex">
          <Breadcrumb.Item href="/" className="none-active-bread">
            Home
          </Breadcrumb.Item>

          <Breadcrumb.Item className="text-dark" active>
            Privacy Policy
          </Breadcrumb.Item>
        </Breadcrumb>
      </div> */}
      <div className="mt-5">
        <Container>
          <Row>
            <Col>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                PRIVACY POLICY for ibetinnews
              </h4>
              <p className="color-gray mb-4">
                The PRIVACY POLICY guidelines provide information regarding the manner in which your details is accumulated, stored and employed by us. You are requested to kindly proceed through this PRIVACY POLICY for ibetinnews before proceeding with our services.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Notice:</h4>
              <p className="color-gray mb-4">
                Our online privacy policy may change anytime without previous notification. To make certain that you know about any changes, kindly review the guidelines periodically. This ONLINE PRIVACY POLICY shall apply uniformly to ibetinnews mobile application. The views and predictions are for entertainment purpose only and should not be seen as official detail. We are not responsible for anyone misusing the statistics and predictions that we give from time to time.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">General</h4>
              <p className="color-gray mb-4">
                We won't be selling, posting or booking your private data to any third party or use your email address/mobile no. for promotional e-mails or Text message. The e-mails and SMS that'll be dispatched by ibetinnews Collection will only have reference to the provision of arranged services in line with the Privacy Policy. Periodically, we might reveal standard statistical information about ibetinnews Range and its own users, such as amount of visitors, amount and kind of goods and services purchased, etc.<br></br>
                We reserve the right to communicate your individual information to any alternative party which makes a legally-compliant obtain its disclosure.
              </p>
              <p className="color-gray mb-4">
                We reserve the right to communicate your individual information
                to any alternative party which makes a legally-compliant obtain
                its disclosure.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Personal Information
              </h4>
              <p className="color-gray mb-4">
                PRIVATE INFORMATION means and includes all information that may be linked to a particular individual or even to identify anybody, such as name, mobile number and any details that might have been voluntarily provided by an individual regarding the availing the services on ibetinnews Collection.PRIVATE INFORMATION means and includes all information that may be linked to a particular individual or even to identify anybody, such as name, mobile number and any details that might have been voluntarily provided by an individual regarding the availing the services on ibetinnews Collection.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Usage of Personal Information
              </h4>
              <p className="color-gray mb-4">
                We use private information for creating individual identify of yourself as a new player. We put it to use to offer you services you explicitly wanted for, assess your interest inside our services, let you know about matches, competitions, booking services, changes, customise your experience, find and drive back mistake, enforce our conditions and conditions, etc.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Security</h4>
              <p className="color-gray mb-4">
                ibetinnews Pros has strict security methods in order to protect losing, misuse, and alteration of the info under our control. Once you change or gain access to your username and passwords, we offer the utilization of our secure server. Once your detail is inside our possession we abide by strict security suggestions, guarding it against unauthorized gain access.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Consent</h4>
              <p className="color-gray mb-5">
                Through the use of ibetinnews and/or by giving your details, you consent to the use of info you disclose on ibetinnews Series related to this PRIVACY POLICY guidelines, including however, not limited by your consent for showing your information according to this online privacy policy.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Privacy;
