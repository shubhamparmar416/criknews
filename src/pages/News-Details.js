import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { NEWS_API_KEY, NEWS_URL, API_URL,APP_DOMAIN ,HEADERS} from "../constant";
import {Card, Container,  Row,  Col} from "react-bootstrap";
import moment from 'moment';
import { shareLink } from "../helpers/utils";
import axios from "axios";
const NewsDetails = () => {
  const navigate = useNavigate();
  const [news, setNews] = useState(null);
  const [seriesMatchList, setSeriesMatchList] = useState(null);
  const [trendingNews, setTrendingNews] = useState(null);
  useEffect(() => {
    const getData = async () => {
      try {
        const matchSeriesResponse = await axios.get(
          `${API_URL}getSeries`,
          HEADERS
        );
        let result = matchSeriesResponse?.data?.data ?? [];
        setSeriesMatchList(result);
      } catch (err) {
      } finally {
      }
    };
    const getTrendingData = async () => {
        try {
            const date = new Date();
            date.setDate(date.getDate() - 1)
            const yesterday = date.toLocaleDateString('en-US');
            const newsUrl = `${NEWS_URL}top-headlines?country=in&category=sports&apiKey=${NEWS_API_KEY}&sortBy=popularity`;
            const url = `${API_URL}callNewsApi`;
            const response = await axios.post(url, { url: newsUrl });
            setTrendingNews(response.data.articles);
        } catch (err) {
        } finally {
        }
    };
    getData();
    getTrendingData();
    getLocalStorageNews();
    moment(news?.publishedAt).format("DD/MM/YYYY");
  }, []);
  const getLocalStorageNews = async () => {
    const localNews = localStorage.getItem("newsItem");
    if (localNews) {
      setNews(JSON.parse(localNews));
    }
  };
  
  const handleReadMore = (news) => {
    localStorage.setItem("newsItem", JSON.stringify(news));
    window.location.reload(true);
  };
  return (
    <div className="fact-area-3 intro-area bg-navy-2 pd-bottom-90 mt-0">
      <Container>
        {/* <h3 className="text-white mb-0">Top News</h3> */}
        <Row>
          <Col sm={9}>
            <Card className="news-detailscrd rounded-4 border-0  bg-white">
              <Card.Img
                className="rounded-4  n-detmg"
                variant="top"
                src={news?.urlToImage}
              />
              <Card.Body className="details">
                <Row>
                  {" "}
                  <Col className="">

                  </Col>
                  <Col className="text-right pt-3">
                    <div className="d-flex n-detp-left">

                      <img
                        className="cursur-pointer"
                        src="assets/img/back.png"
                        alt="Back" onClick={() => navigate("/")}
                      />
                    </div>
                    <div className="d-flex n-detp cursur-pointer">

                      <img
                        className="cursur-pointer"
                        src="assets/img/share.png"
                        alt="Share" onClick={() => shareLink()}
                      />
                    </div>
                  </Col>
                </Row>
                <p>Cricket</p>
                <h1 className="text-clamps">{news?.title}</h1>
                <p className="ft-size-12 text-black py-2">
                  {moment(news?.publishedAt).format("DD MMMM YYYY - hh:mm A, dddd")}
                </p>{" "}
                <Card.Text className="ft-size-15 Text-black">{news?.description}
                </Card.Text>
                {/* <div className="mt-4">
                  <span className="ft-size-15 bg-gray-tag">
                    Live cricket
                  </span></div> */}
              </Card.Body>
            </Card>
          </Col>
          <Col sm={3} >
            <div>
              <div className="d-flex mt-5 align-items-center"><span className="blckog"></span>
                <h4 className="mb-0">Top Stories</h4></div>
              <Row className="mt-3 bg-white rounded-4 py-1">
                {trendingNews?.slice(0, 3).map((item, index) => {
                  return (
                    <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2 cursur-pointer" onClick={() => handleReadMore(item)}>{item.title} <br /> {moment(item?.publishedAt).format("Y-m-d")}</p>
                  );
                })}

                {/* <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2"> 2023 Asia cup <br />2023-09-02</p>

                <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2"> 2023 Asia cup <br />2023-09-02</p> */}
              </Row>
            </div>
            <div><div className="d-flex mt-5 align-items-center"><span className="blckog"></span>
              <h4 className="mb-0">Trending</h4></div>
              <Row className="mt-3 bg-white rounded-4 py-1">
                {trendingNews?.slice(3, 6).map((item, index) => {
                  return (
                    <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2 cursur-pointer" onClick={() => handleReadMore(item)} >{item.title} <br />{moment(item?.publishedAt).format("Y-m-d")}</p>
                  );
                })}
                {/* <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2"> 2023 Asia World cup <br />2023-09-02</p>
                <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2"> 2023 Asia cup <br />2023-09-02</p>

                <p class="ft-size-12 text-black fw-bold border-bottom-grays py-2"> 2023 Asia cup <br />2023-09-02</p> */}
              </Row>
            </div>
          </Col>
          <Col sm={12} className="">
            <img className="rounded-lg-5 w-100 mb-4" src="assets/img/imgpsh_fullsize_anim_1.png" alt="Team" />

          </Col>
        </Row>

      </Container>
      {/* <Container className="mt-5 pb-5 homecare ">
        <Row className="pb-3">
          <Col sm={6}>
            <h3 className="text-white mb-0">Featured Videos</h3>
          </Col>
          <Col sm={6} className="text-right">
            <Button
              className=" btn-base py-2 px-4 h-auto ft-size-15 btn btn-primary"
              onClick={() => navigate("/featured-videos")}
            >
              View All
            </Button>
          </Col>
        </Row>
        <Carousel className="side-homes d-none-arrow">
          <Carousel.Item>
            <Row>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
            </Row>
          </Carousel.Item>
          <Carousel.Item>
            <Row>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
            </Row>
          </Carousel.Item>
          <Carousel.Item>
            <Row>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
              <Col sm={4}>
                <TrendingCard />
              </Col>
            </Row>
          </Carousel.Item>
        </Carousel>
      </Container> */}
    </div>
  );
};

export default NewsDetails;
