import React, { useState } from "react";
import { Button, Container, Row, Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
const SignUp = () => {
  const [fullName, setFullName] = useState(null);
  const [email, setEmail] = useState(null);
  const [mobile, setMobile] = useState(null);
  const [password, setPassword] = useState(null);
  const [validated, setValidated] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");

  const handleInputChange = (e) => {
    console.log(e.target.id);
    if (e.target.id === 'fullName') {
      setFullName(e.target.value);
    }
    else if (e.target.id === 'email') {
      setEmail(e.target.value);
    }
    else if (e.target.id === 'mobile') {
      setMobile(e.target.value);
    }
    else if (e.target.id === 'password') {
      setPassword(e.target.value);
    }



  }
  const reset = () => {
    setFullName('');
    setEmail('');
    setMobile('');
    setPassword('');


  }

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    setValidated(true);
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      try {
        event.preventDefault();
        const response = await fetch('https://api.storelyapp.com:5000/v1/signup', {
          method: 'POST',
          headers: {
            'Accept': "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ fullName: fullName, email: email, mobile: mobile, password: password })
        });
        const data = await response.json();
        if (data.status === 'success') {
          reset();
          setErrorMessage('');
          setSuccessMessage(data.message)
        }
        else {
          setErrorMessage(data.message);
        }

        console.log(data.status)

      } catch (err) {
      } finally {
        // setLoading(false);
      }
    }
  }


  return (

    <div className="fact-area-3 intro-area  pd-top-120 pd-bottom-90 login-bg">
      <Container>
        <Row className="pb-5 justify-content-center align-items-center">
          <Col sm={4}>
          </Col>
          <Col sm={4}>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
              <div className="text-center">

                <img src="assets/img/signin.svg" alt="" style={{ width: "150px" }} />
                <h2 className="text-white mb-3">Register</h2>
              </div>
              {errorMessage && (
                <p className="error"> {errorMessage} </p>
              )}
              {successMessage && (
                <p className="success"> {successMessage} </p>
              )}
              <Form.Group
                className="mb-3 single-input-inner style-border text-center"
                controlId="fullName"
              >
                <Form.Control
                  type="text"
                  required minLength={3} maxLength={30}
                  placeholder="FullName" value={fullName} onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group
                className="mb-3 mt-3 single-input-inner style-border"
                controlId="email"
              >
                <Form.Control
                  type="email"
                  required
                  placeholder="Email Id" value={email} onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="mobile"
              >
                <Form.Control
                  type="text" required
                  placeholder="Phone" value={mobile} onChange={handleInputChange}
                />
              </Form.Group>

              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="password"
              >
                <Form.Control type="password" required placeholder="Password" value={password} onChange={handleInputChange} />

                <div className="eye-icn">
                  <svg
                    width="22"
                    height="23"
                    viewBox="0 0 22 23"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g opacity="0.6">
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M13.8976 11.2928C13.8976 12.8933 12.5996 14.1904 10.9991 14.1904C9.39865 14.1904 8.10156 12.8933 8.10156 11.2928C8.10156 9.69137 9.39865 8.39429 10.9991 8.39429C12.5996 8.39429 13.8976 9.69137 13.8976 11.2928Z"
                        stroke="#BBBBBB"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        strokeLinejoin="round"
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M10.9978 17.9861C14.4885 17.9861 17.6813 15.4763 19.4788 11.2926C17.6813 7.10895 14.4885 4.59912 10.9978 4.59912H11.0015C7.51084 4.59912 4.31809 7.10895 2.52051 11.2926C4.31809 15.4763 7.51084 17.9861 11.0015 17.9861H10.9978Z"
                        stroke="#BBBBBB"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        strokeLinejoin="round"
                      />
                    </g>
                  </svg>
                </div>
              </Form.Group>
              <Row className="align-items-center">

                <Col sm={12}>
                  <Button
                    className="w-100 btn-base py-3 h-auto ft-size-15 fw-bold"
                    variant="primary"
                    type="submit"
                  >
                    Sign Up
                  </Button>
                </Col>
              </Row>
            </Form>
            <Row className="text-center py-3">
              <Col sm={12}>

                {/* <div className="d-flex google-custom justify-content-center">

                </div> */}
                <p className="color-gray ">
                  Already have an account ?
                  <span>
                    <Link
                      className="text-white text-decoration-none ps-2"
                      to="/login"
                    >
                      Sign In
                    </Link>
                  </span>
                </p>
                <div className="line-sep">
                  <p className="color-gray py-3">Or continue with</p>
                </div>
                <div className="">
                  <img className="me-2" src="assets/img/Google.svg" alt="" />

                  <img src="assets/img/Facebook.svg" alt="" />
                </div>
                <p class="color-gray ft-size-11 text-center pt-3">By sign in with Google or Facebook, you agree to<br />ibetinnews Terms and Privacy policies.</p>
              </Col>
            </Row>
          </Col>
          <Col sm={4}>
            <Row>

            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default SignUp;
