import React from "react";
import { Button, Container, Row, Col, Breadcrumb, Form } from "react-bootstrap";
const Contact = () => {
  return (
    <div className="fact-area-3 intro-area bg-navy-2 pd-top-120 pd-bottom-90">
      <div className="breadcrumb-inner text-center ">
        <h1 className="page-title text-white">CONTACT US</h1>
        <Breadcrumb className="justify-content-center d-flex">
          <Breadcrumb.Item href="/" className="none-active-bread">
            Home
          </Breadcrumb.Item>

          <Breadcrumb.Item className="text-light" active>
            CONTACT US
          </Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <Container>
        <Row className="pb-5">
          <Col sm={6}>
            <Form>
              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="formBasicText"
              >
                <h4 className="text-light mb-3">CONTACT US</h4>

                <Form.Label className="subtitle mb-3">
                  Welcome to help center please Describe the problem.
                </Form.Label>

                <Form.Control type="text" placeholder="Full Name" />
                <div className="contactsvg">
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M9.9873 12.7885C6.76428 12.7885 4.0119 13.2758 4.0119 15.2273C4.0119 17.1789 6.74682 17.6837 9.9873 17.6837C13.2103 17.6837 15.9619 17.1956 15.9619 15.2448C15.9619 13.294 13.2278 12.7885 9.9873 12.7885Z"
                      stroke="#01C29A"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M9.98729 10.0049C12.1024 10.0049 13.8167 8.28978 13.8167 6.1747C13.8167 4.05962 12.1024 2.34534 9.98729 2.34534C7.87221 2.34534 6.15713 4.05962 6.15713 6.1747C6.14998 8.28264 7.85316 9.99772 9.9603 10.0049H9.98729Z"
                      stroke="#01C29A"
                      strokeWidth="1.42857"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </div>
              </Form.Group>

              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="formBasicEmail"
              >
                <Form.Control type="email" placeholder="Email Address" />
                <div className="contactsvg">
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M14.9189 7.37598L11.2161 10.3868C10.5165 10.9418 9.53226 10.9418 8.83268 10.3868L5.09869 7.37598"
                      stroke="#01C29A"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M14.0907 17.5C16.6252 17.507 18.3334 15.4246 18.3334 12.8653V7.14168C18.3334 4.58235 16.6252 2.5 14.0907 2.5H5.9093C3.37484 2.5 1.66669 4.58235 1.66669 7.14168V12.8653C1.66669 15.4246 3.37484 17.507 5.9093 17.5H14.0907Z"
                      stroke="#01C29A"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </div>
              </Form.Group>

              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="exampleForm.ControlTextarea1"
              >
                {" "}
                <Form.Control placeholder="Message" as="textarea" rows={3} />
                <div className="contactsvgtext">
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M13.6118 2.29175H6.38764C3.87014 2.29175 2.29181 4.07425 2.29181 6.59675V13.4034C2.29181 15.9259 3.86181 17.7084 6.38764 17.7084H13.611C16.1368 17.7084 17.7085 15.9259 17.7085 13.4034V6.59675C17.7085 4.07425 16.1368 2.29175 13.6118 2.29175Z"
                      stroke="#01C29A"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M13.2828 10.0108H13.2903"
                      stroke="#01C29A"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M9.94197 10.0108H9.94947"
                      stroke="#01C29A"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M6.60115 10.0108H6.60865"
                      stroke="#01C29A"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </div>
              </Form.Group>

              <Button
                className="w-100 btn-base py-3 h-auto ft-size-15 fw-bold"
                variant="primary"
                type="submit"
              >
                Send
              </Button>
            </Form>
          </Col>
          <Col sm={6}>
            <Row>
              <h4 className="text-light mb-4 mt-5 mt-lg-0">
                For More Information
              </h4>
              <Col sm={6}>
                <div className="single-plan-wrap">
                  <h5 className="text-light">Address:</h5>
                  <ul className="mt-1 botoom-li list-unstyled">
                    <li className="text-light">
                      <img
                        className="pe-2 "
                        src="assets/img/marker.png"
                        alt="Location"
                      />
                      Business Bay, DUBAI
                    </li>
                  </ul>
                </div>
              </Col>
              <Col sm={6}>
                <div className="single-plan-wrap">
                  <h5 className="text-light">Mail:</h5>
                  <ul className="mt-1 botoom-li list-unstyled">
                    <li className="text-light">
                      <span className="icon">
                        <i className="fa fa-envelope"></i>
                      </span>
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M14.9189 7.37598L11.2161 10.3868C10.5165 10.9418 9.53226 10.9418 8.83268 10.3868L5.09869 7.37598"
                          stroke="#01C29A"
                          strokeWidth="1.5"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M14.0907 17.5C16.6252 17.507 18.3334 15.4246 18.3334 12.8653V7.14168C18.3334 4.58235 16.6252 2.5 14.0907 2.5H5.9093C3.37484 2.5 1.66669 4.58235 1.66669 7.14168V12.8653C1.66669 15.4246 3.37484 17.507 5.9093 17.5H14.0907Z"
                          stroke="#01C29A"
                          strokeWidth="1.5"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>{" "}
                      info@cricketinsomnia.com
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Contact;
