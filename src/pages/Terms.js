import React from "react";
import { Container, Row, Col } from "react-bootstrap";
const Terms = () => {
  return (
    <div className="fact-area-3 intro-area bg-navy-2 mt-0">
      {/* <div className="breadcrumb-inner text-center ">
        <h1 className="page-title text-white">Terms & Conditions</h1>
        <Breadcrumb className="justify-content-center d-flex">
          <Breadcrumb.Item href="/" className="none-active-bread">
            Home
          </Breadcrumb.Item>

          <Breadcrumb.Item className="text-dark" active>
            Terms & Conditions
          </Breadcrumb.Item>
        </Breadcrumb>
      </div> */}
      <div className="mt-5">
        <Container>
          <Row>
            <Col>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Terms of Use</h4>
              <p className="color-gray mb-4">
                These Terms of Use constitute and govern the relationship
                between ‘Provider’ (defined below) and all parties who access
                the Website/Mobile App (defined hereunder) and make use of its
                features and contents in any manner (hereafter referred to as
                ‘Visitors’). Collectively the Provider and the Visitors shall be
                referred to as the ‘Parties’
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">General</h4>
              <p className="color-gray mb-4">
                The use of any or all of the features and services offered by
                the Provider on cricbuzy.com website/Mobile App (hereafter
                referred to as the ‘Website/Mobile App’) and the information,
                materials and links contained therein, is subject to the Terms
                of Use as set out below. Unless otherwise agreed by the Provider
                in writing, the Terms of Use constitute the entire relationship
                between the Provider and the Visitor in its use of the
                Website/Mobile App including any or all of its functions on
                offer by the Website/Mobile App.
              </p>
              <p className="color-gray mb-4">
                The Visitor has the duty to read carefully and understand the
                Terms of Use before using the Website/Mobile App. A Visitor who
                has viewed the Website/Mobile App is considered to have read,
                understood and agreed to be bound by the Terms of Use, without
                the need for any further act.
              </p>
              <p className="color-gray mb-4">
                The Provider hereby reserves the right to suspend, add, end,
                amend and/or supplement these Terms of Use from time to time as
                it may deem appropriate.
              </p>
              <p className="color-gray mb-4">
                The Provider recommends that the Visitor reads carefully the
                contents of these pages regularly. By using the Website/Mobile
                App the Visitor agrees to be bound by the Terms of Use, as well
                as by the latest modifications to them, regardless of whether in
                fact the Visitor is aware of such modifications.
              </p>
              <p className="color-gray mb-4">
                The Provider is under no obligation to verify that all Visitors
                use the Website/Mobile App according to the last updated Terms
                of Use. The effective version of Terms of Use is that which is
                posted on the Website/Mobile App.
              </p>
              <p className="color-gray mb-4">
                The Website/Mobile App may only be used for lawful purposes. Use
                of the Website/Mobile App for transmission, distribution,
                publication or storage of any material on or via the
                Website/Mobile App which is in violation of any applicable law
                or regulation or any third party's rights is strictly
                prohibited. This includes (without limitation) the use of the
                Website/Mobile App or the transmission, distribution,
                publication or storage of any material on or via the
                Website/Mobile App in a matter or for a purpose which infringes
                copyright, trademark, trade secret or other intellectual
                property rights, is obscene or harmful to minors or constitutes
                an illegal act or harassment, is libellous or defamatory,
                violates any privacy or data protection laws, is fraudulent or
                breaches any exchange control or gambling law.
              </p>
              <p className="color-gray mb-4">
                In the event of misuse and/or the abuse of the Website/Mobile
                App, the Provider reserves the right to close or block the
                Visitor from the Website/Mobile App and close any account
                registered in the Visitor’s name. The provider retains the right
                to bring a lawsuit against the Visitor and at its sole
                discretion.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Services</h4>
              <p className="color-gray mb-4">
                The Website/Mobile App provide live sports information in
                sporting events, sports scores in real time, final results,
                fixtures, lineups and sports statistics. The results, and other
                statistics information contained on the Website/Mobile App
                reflect information provided by other independent sources (from
                third parties) or by in-house effort or by various other
                official websites. While every effort is made by the Provider to
                update the content and match results or other information
                displayed on the Website/Mobile App regularly, we advise to
                double check information gathered on Website/Mobile App also
                from other sources. The Provider is not responsible for the
                Visitor’s use of the results and other information contained on
                the Website/Mobile App.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Third party websites
              </h4>
              <p className="color-gray mb-4">
                The Visitor acknowledges that any contact whatsoever made with
                third parties after viewing the Website/Mobile App, whether
                intended or unintended, and any outcome which ensues, is
                absolutely independent of the Provider and the Provider is not
                in any way responsible for any agreement or expectation and
                other consequence which ensues as a direct or indirect cause of
                this contact.
              </p>
              <p className="color-gray mb-4">
                Any claim or dispute which may arise between the Visitor and
                such a third party shall in no way involve the Provider. Third
                parties, including any third parties advertising on the
                Website/Mobile App do not have access to the Visitors’ Personal
                Data and any other data that the Visitor may have given to the
                Provider.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">A/V Content</h4>
              <p className="color-gray mb-4">
                The Provider is not responsible for the content of external
                websites which may be viewed from the Website/Mobile App. All
                video content found on the Website/Mobile App is not hosted on
                the Provider’s servers nor is it created or uploaded to the host
                server by the Provider.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Inactive Accounts
              </h4>
              <p className="color-gray mb-4">
                If a Visitor has set up an account on the Website/Mobile App but
                fails to access it for 60 days, the Provider reserves the right
                to close the account with immediate effect and without prior
                notice.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Type of Relationship
              </h4>
              <p className="color-gray mb-4">
                These Terms of Use are not intended to create any partnership,
                agency or joint venture between the Provider and the Visitor.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Breach of Agreement
              </h4>
              <p className="color-gray mb-4">
                If the Visitor fails to adhere to any clause in the Terms of Use
                or if the Provider reasonably suspect that a Visitor whether
                directly or indirectly fails to comply with any clause in the
                Terms of Use, the Provider reserves the right, and all remedies
                at its disposition, and at its sole discretion, to close or
                block the Visitor from the Website/Mobile App and close any
                account registered in the Visitor’s name and related to it and
                retains the right to bring a lawsuit against the Visitor and at
                its sole discretion.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Legal Compliance</h4>
              <p className="color-gray mb-4">
                Visitors are advised to comply with applicable legislation in
                the jurisdiction in which they are domiciled and/or resident
                and/or present. The Provider does not accept responsibility for
                any action taken by any authority against any Visitor in
                connection with their use of the Website/Mobile App.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Law & Forum and/or communityLaw & Forum and/or community
              </h4>
              <p className="color-gray mb-4">
                This Agreement shall be governed by and construed in accordance
                with the laws of the country of India without giving effect to
                conflicts of law principles. The Parties submit to the exclusive
                jurisdiction of the court of the country of India, Delhi for the
                settlement of any disputes arising out of concerning this
                Agreement.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Headings</h4>
              <p className="color-gray mb-4">
                Headings are intended for clarity and to facilitate reading of
                these Terms of Use. They are not intended as a means of
                interpretation for the content of the paragraph that follows
                each heading. Headings are not intended to bind the Provider in
                any manner whatsoever.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Waiver</h4>
              <p className="color-gray mb-4">
                Any waiver by the Provider of any breach by any Visitor of any
                provision of these Terms of Use shall not be considered as a
                waiver of any subsequent breach of the same or any other
                provision of these Terms of Use.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">
                Disclaimer Warranties and Representations
              </h4>
              <p className="color-gray mb-4">
                It is hereby being specified that the Provider makes no
                representation, pledge or warranty (either explicit or implicit)
                that the content of the Website/Mobile App is accurate and/or
                suitable for any particular purpose other than those warranties
                which cannot be expressly excluded under the governing law of
                these Terms of Use.
              </p>
              <p className="color-gray mb-4">
                Use of the Website/Mobile App is entirely at the Visitor's risk.
                The Website/Mobile App is not a gaming or gambling
                website/Mobile App. The Provider of the Website/Mobile App does
                not provide gaming or gambling services, therefore it does not
                hold or control player funds and it is not involved in any
                gaming transactions. Odds which are displayed on the
                Website/Mobile App are part of information and functions of the
                Website/Mobile App.
              </p>
              <p className="color-gray mb-4">
                The Provider does not guarantee that any of the functions
                provided by the Website are authorised, and that the operation
                will fully satisfy the Visitor, that it is entirely secure and
                exempt from error, that it is updated regularly, that any
                software defect is regularly corrected, that it is
                uninterrupted, that the Website/Mobile App are virus or bug
                free, or that they are continually operational, that they are
                adequate, that the information and functions available thereon
                is reliable, or that all other information obtained and
                functions used on the Website/Mobile App are adequate and
                reliable. Those who choose to access the Website/Mobile App do
                so on their own initiative and are responsible for compliance
                with local laws, if and to the extent local laws are applicable.
              </p>
              <p className="color-gray mb-4">
                The Website/Mobile App may contain links and references to third
                party websites/adverts/content. These are provided for the
                convenience and interest of the Visitor and does not imply
                responsibility for, nor approval of, information contained in
                these websites adverts/content by the Provider. The Provider
                gives no warranty, either expressed or implied, as to the
                accuracy, availability of content or information, text or
                graphics which are not under its domain. The Provider has not
                tested any software located on other websites and does not make
                any representation as to the quality, safety, reliability or
                suitability of such software.
              </p>
              <h4 className="text-dark mb-4 mt-5 mt-lg-0">Loss or Damage</h4>
              <p className="color-gray mb-4">
                The Provider is not responsible for any loss or damage, direct
                or indirect, that the Visitor or a third party might have
                suffered as a result of using the Website/Mobile App, including
                but not limited to damages caused by a commercial loss, a loss
                of benefits, a loss on anticipated earnings, winnings or other
                profit, interruption of business, loss of commercial
                information, or any other pecuniary and or consecutive loss.
              </p>
              <p className="color-gray mb-4">
                The Provider is not responsible for winnings made or losses
                suffered on third party websites which result from the use of
                information displayed on the Website/Mobile App.
              </p>
              <p className="color-gray mb-4">
                Without limitation to the generality of the preceding two
                clauses, no responsibility is being acknowledged or accepted
                hereunder for, inter alia, the following matters: mistake(s),
                misprint(s), misinterpretation(s), mishearing(s), misreading(s),
                mistranslation(s), spelling mistake(s), fault(s) in reading,
                transaction error(s), technical hazard(s), registration
                error(s), manifest error(s), Force(s) Majeure and/or any other
                similar mistake(s)/error(s); violation of the Provider’s rules;
              </p>
              <p className="color-gray mb-4">
                criminal actions; advice, in whichever form, provided by the
                Provider; legal actions and/or other remedies; loss or damage
                that Visitors or third parties might have suffered as a result
                of their use of the Website/Mobile App, its content or that of
                any link suggested by the Provider; loss or damage that Visitors
                or third parties might have suffered as a result of any
                modification, suspension or interruption of the Website;
                criminal use of the Website or of its content by any person, of
                a defect, or omission or of any other factor beyond our control;
                any use made of the Website due to a third party accessing the
                private areas requiring login and password by using a Visitor’s
                Username and Password; in case of discrepancies in the services,
                functions and any other feature offered by the Website due to
                viruses or bugs as it relates to all parameters that make up the
                Website, any damage, costs, expenses, losses, or claims brought
                about by said discrepancies; any act or omission by an internet
                provider or of any other third party with whom Visitors may have
                contracted in order to have access to the Website. In case of
                litigation between the internet provider and Visitors, the
                Provider cannot be a party to the suit, and such suit shall in
                no way affect these Terms of Use.
              </p>
              <p className="color-gray mb-5">
                Any claim arising as a result of damages incurred by a Visitor
                due to the content of any material posted by another Visitor or
                other third party not authorised by the Provider on the
                Website/Mobile App.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Terms;
