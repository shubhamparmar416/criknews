import React, { useState } from "react";
import { Button, Container, Row, Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
const Login = () => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [validated, setValidated] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");

  const handleInputChange = (e) => {
    console.log(e.target.id);
    if (e.target.id === 'email') {
      setEmail(e.target.value);
    }
    else {
      setPassword(e.target.value);
    }
  }
  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    setValidated(true);
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      try {
        event.preventDefault();
        const response = await fetch('https://api.storelyapp.com:5000/v1/login', {
          method: 'POST',
          headers: {
            'Accept': "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email: email, password: password })
        });
        const data = await response.json();
        if (data.status === 'success') {
          setSuccessMessage(data.message)
        }
        else {
          setErrorMessage(data.message);
        }


      } catch (err) {
      } finally {
        // setLoading(false);
      }
    }
  }


  return (
    <div className="fact-area-3 intro-area  pd-top-120 pd-bottom-90 login-bg">
      <Container>
        <Row className="pb-5 justify-content-center align-items-center">
          <Col sm={4}>
          </Col>
          <Col sm={4}>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
              <div className="text-center">
                <img src="assets/img/signin.svg" alt="" style={{ width: "150px" }} />
                <h2 className="text-white mb-3">Sign in</h2></div>
              {errorMessage && (
                <p className="error"> {errorMessage} </p>
              )}
              {successMessage && (
                <p className="success"> {successMessage} </p>
              )}
              <Form.Group
                className="mb-3 single-input-inner style-border text-center"
                controlId="email"
              >
                <Form.Control
                  type="email" required
                  placeholder="Email" value={email} onChange={handleInputChange}
                />

              </Form.Group>

              <Form.Group
                className="mb-3 single-input-inner style-border"
                controlId="password"
              >
                <Form.Control type="password" required placeholder="Password" value={password} onChange={handleInputChange} />

                <div className="eye-icn">
                  <svg
                    width="22"
                    height="23"
                    viewBox="0 0 22 23"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g opacity="0.6">
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M13.8976 11.2928C13.8976 12.8933 12.5996 14.1904 10.9991 14.1904C9.39865 14.1904 8.10156 12.8933 8.10156 11.2928C8.10156 9.69137 9.39865 8.39429 10.9991 8.39429C12.5996 8.39429 13.8976 9.69137 13.8976 11.2928Z"
                        stroke="#BBBBBB"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        strokeLinejoin="round"
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M10.9978 17.9861C14.4885 17.9861 17.6813 15.4763 19.4788 11.2926C17.6813 7.10895 14.4885 4.59912 10.9978 4.59912H11.0015C7.51084 4.59912 4.31809 7.10895 2.52051 11.2926C4.31809 15.4763 7.51084 17.9861 11.0015 17.9861H10.9978Z"
                        stroke="#BBBBBB"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        strokeLinejoin="round"
                      />
                    </g>
                  </svg>
                </div>
              </Form.Group>
              <Row className="align-items-center">
                <Col sm={12}>
                  <Link className="color-gray text-decoration-none text-right" to="/">
                    Forgot Password?
                  </Link>
                </Col>
                <Col sm={12}>
                  <Button
                    className="w-100 btn-base py-3 h-auto ft-size-15 fw-bold"
                    variant="primary"
                    type="submit"
                  >
                    Login
                  </Button>
                </Col>
              </Row>
            </Form>

            <Row className="text-center py-3">
              <Col sm={12}>
                {/* 
                <div className="d-flex google-custom justify-content-center">

                </div> */}
                <p className="color-gray ">
                  Don’t have an account?
                  <span>
                    <Link
                      className="text-white text-decoration-none ps-2"
                      to="/signup"
                    >
                      Sign Up
                    </Link>
                  </span>
                </p>
                <div className="line-sep">
                  <p className="color-gray py-3">or continue with</p>
                </div>
                <div className="">
                  <img className="me-2" src="assets/img/Google.svg" alt="" />

                  <img src="assets/img/Facebook.svg" alt="" />
                </div>
              </Col>
            </Row>
          </Col>
          <Col sm={4}>

          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
