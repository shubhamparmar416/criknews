import React, { useEffect, useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";
import RecentNewsCarousel from "../components/RecentNewsCarousel";
import TopStories from "../components/shared/TopStories";
import Trending from "../components/shared/Trending";
import { useNavigate } from "react-router-dom";

import BottomStories from "../components/shared/BottomStories";
const Home = () => {
  const navigate = useNavigate();

  return (
    <div className="fact-area-3 intro-area homecare  pd-bottom-90 mt-2">
      <Container>
        <Row className="">
          <Col sm={12} className="">
            <img className="rounded-lg-5 cursur-pointer" src="assets/img/imgpsh_fullsize_anim_new.png" alt="Team" onClick={() => navigate("/signup")} />
          </Col>
        </Row>
      </Container>

      <Row className="">
        <Col sm={12} className="mb-5 mt-4">
          <RecentNewsCarousel />
        </Col>
      </Row>
      <Trending />
      <Container>
        <Row className="">
          <Col sm={12} className="">
            <img className="rounded-lg-5 w-100 mb-4 cursur-pointer" src="assets/img/imgpsh_fullsize_anim_4.png" alt="Team" onClick={() => navigate("/signup")} />

          </Col>
        </Row>
      </Container>

      <TopStories />
      <BottomStories />


      {/* </Container> */}
      {/* <TopNews /> */}
    </div>
  );
};

export default Home;
