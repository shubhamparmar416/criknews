export const APP_DOMAIN = process.env.REACT_APP_DOMAIN;
export const API_URL = process.env.REACT_APP_BASE_URL;
export const ASSET_URL = process.env.REACT_APP_ASSET_URL;
export const APP_NAME = process.env.REACT_APP_NAME;
export const RANOZE_URL = process.env.REACT_APP_RANOZE_URL;
export const NEWS_URL = process.env.REACT_APP_NEWS_URL;
export const NEWS_API_KEY = process.env.REACT_APP_NEWS_API_KEY;
export const HEADERS = {
  headers: {
    "Content-Type": "application/json",
    Authorization:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im11a2VzaDE0NzcxQGdtYWlsLmNvbSIsInJlZnJlc2hUb2tlbiI6Ik9VbHphMXJndWpsUTRhZWdDWDRYUkZDRE1iWUlmSEJ3dUl1dEVIRDR5V0xnbUxWTkhLWG5Nam5uVmpOZW1La1ltS1VRd2pRMkVYeWpqOU4xcW1XM082SnZIeDhLc1pFYnlUQ2gxNFF1NnNWb2k1MmVBVFZuNjlsNThlM2xKNGZTIiwidGltZSI6IjIwMjMtMDItMThUMTY6MTY6NTIuODQxWiIsImlhdCI6MTY3NjczNzAxNCwiZXhwIjoxNzA4MjczMDE0fQ.teX_jd3-AP1DrlnNe0hU6eD1Xf1GT-6iwNP0yn0o6JQ",
  },
};
export const THE_SPORT_URL = process.env.REACT_APP_THE_SPORT_URL;
export const SOCKET_URL = process.env.REACT_APP_SOCKET_URL;
export const SPORT_USER = process.env.REACT_APP_SOCKET_USER;
export const SPORT_SECRET = process.env.REACT_APP_SOCKET_SECRET;
export const CRIPCO_API_URL = process.env.REACT_APP_CRIPCO_API_URL;
export const TEAM_THUMBNAIL = "flag.png";
export const PLACEHOLDER_THUMBNAIL = "placeholder.jpg";
export const BLOG_THUMBNAIL = "news.png";
export const SERIES_THUMBNAIL = "news.png";
export const NEWS_THUMBNAIL = "news.png";
export const PLAYER_THUMBNAIL = "player.png";
export const VIDEO_THUMBNAIL = "news.png";
/**
 * 0 - 100 denote interrupt or abort excludng 1.
 * 1 yet to start.
 * 100 match completed.
 * 500 denot match is live.
 */
export const MATCH_STATE = [
  { 0: "ABNORMAL" },
  { 1: "NOT_STARTED" },
  { 532: "FIRST_INNINGS_HOME_TEAM" },
  { 533: "FIRST_INNINGS_AWAY_TEAM" },
  { 534: "SECOND_INNINGS_HOME_TEAM" },
  { 535: "SECOND_INNINGS_AWAY_TEAM" },
  { 536: "AWAITING_SUPER_OVER" },
  { 537: "SUPER_OVER_HOME_TEAM" },
  { 538: "SUPER_OVER_AWAY_TEAM" },
  { 539: "AFTER_SUPER_OVER" },
  { 540: "INNINGS_BREAK" },
  { 541: "SUPER_OVER_BREAK" },
  { 542: "LUNCH_BREAK" },
  { 543: "TEA_BREAK" },
  { 544: "End" },
  { 100: "ENDED" },
  { 14: "POSTPONED" },
  { 15: "DELAYED" },
  { 16: "CANCELED" },
  { 17: "INTERRUPTED" },
  { 19: "Cut in half" },
  { 99: "To be determined" },
];

export const MATCH_TYPE = [
  null,
  "TEST",
  "ODI",
  "T20I",
  "T20",
  "LIST A",
  "FRIST CLASS",
];
export const PLAYER_TYPE = ["Starter", "Substitute", "Injury", "Ban"];
export const ELEMINATE_STATUS = {
  1: "Bowled",
  2: "Caught",
  3: "CaughtBowled",
  4: "Lbw",
  5: "RunOut",
  6: "Stumped",
  7: "Handled",
  8: "HitWicket",
  9: "RetiredHurt",
  10: "RetiredOut",
  11: "HitBallTwice",
  12: "Obstructing",
  13: "TimedOut",
};
export const RECENT_LIMIT = 30;
export const UPCOMING_LIMIT = 3;
