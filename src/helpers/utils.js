import axios from "axios";
import { API_URL, SPORT_SECRET, SPORT_USER, THE_SPORT_URL,APP_DOMAIN } from "../constant";
import moment from 'moment';

import {
  getHistoricalMatchDetail,
  getMatchLineUpDetail,
  getPlayerDetail,
  getSeasonDetail,
  getTeamDetail,
  getTournamentDetail,
  getUniqueTournamentDetail,
  getVenueDetail,
  setHistoricalMatchDetail,
  setMatchLineUpDetail,
  setPlayerDetail,
  setSeasonDetail,
  setTeamDetail,
  setTournamentDetail,
  setUniqueTournamentDetails,
  setVenueDetail,
} from "./cacheData";

export const shareLink = async (news) => {
  const shareData = {
    title: news?.title,
    text: news?.short_detail,
    url: APP_DOMAIN,
  };
  try {
    await navigator.share(shareData);
  } catch (err) { }
};
export const listToGroupList = (list = [], IndividualGroupCount) => {
  let tempList = [];
  let tempSubList = [];
  let count = 1;

  Object.values(list).forEach((val) => {
    tempSubList.push({ ...val });

    if (count === IndividualGroupCount) {
      tempList.push([...tempSubList]);
      tempSubList = [];
      count = 0;
    }

    count++;
  });
  if (list.length < IndividualGroupCount) {
    tempList.push([...tempSubList]);
  }
  return tempList;
};
export const calculateDays = (date1) => {
  let diff = moment.utc(date1).local().startOf('seconds').fromNow();
  diff = diff.replace('ago','');
  return diff;
};
export const limitText = (content,limit) => {
  let text = content.substring(0, limit);
  return text;
};
//will implement later
export const getCatchData = async ({
  method = "get",
  url,
  data,
  time = null,
  key = null,
}) => {
  if (method === "get") {
    return method === "get"
      ? await axios.get(url)
      : await axios.post(url, ...data);
  }
};

export const isEmpty = (str) => !str?.length;
export const isUndefined = (v) => v === undefined;
export const getMatchStatus = (key = null) => {
  let matchStatus = null;
  switch (key) {
    case 1:
      matchStatus = "Start soon";
      break;
    case 532:
    case 533:
    case 534:
    case 535:
    case 536:
    case 537:
    case 538:
    case 539:
    case 540:
    case 541:
    case 542:
    case 543:
      matchStatus = "Live";
      break;
    case 100:
    case 544:
      matchStatus = "Completed";
      break;
    default:
      matchStatus = "Abort";
      break;
  }
  return matchStatus;
};
export const getPlayerPosition = (position) => {
  let modifiedPosition = null;
  switch (position) {
    case "all_rounder":
      modifiedPosition = "All Rounder";
      break;
    case "batsman":
      modifiedPosition = "Batsman";
      break;
    case "bowler":
      modifiedPosition = "Bowler";
      break;
    case "wicket_keeper":
      modifiedPosition = "Wicket keeper";
      break;
    default:
      modifiedPosition = "-";
      break;
  }
  return modifiedPosition;
};

export const fetchVenueDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return null;
  }
  const cacheResult = getVenueDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/venue/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    if (
      response?.data?.results !== undefined &&
      response?.data?.results?.length === 1
    ) {
      setVenueDetail(id, response?.data?.results[0]);
      return response?.data?.results[0];
    } else {
      return null;
    }
  }
};
export const fetchSeasonDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getSeasonDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/season/table/detail?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    if (
      response?.data?.results?.tables !== undefined &&
      response?.data?.results?.tables.length === 1
    ) {
      setSeasonDetail(id, response?.data?.results[0]);
      return response?.data?.results?.tables[0];
    } else {
      return {};
    }
  }
};
export const fetchSeasonList = async (fromDate = null) => {
  const sportUrl = `${THE_SPORT_URL}cricket/season/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&time=${fromDate}`;
  const url = `${API_URL}callSportsApi`;
  const response = await axios.post(url, { url: sportUrl });

  if (
    response?.data?.results !== undefined &&
    response?.data?.results.length > 0
  ) {
    return response?.data?.results;
  } else {
    return null;
  }
};
export const fetchTeamDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getTeamDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/team/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    if (
      response?.data?.results !== undefined &&
      response?.data?.results?.length === 1
    ) {
      setTeamDetail(id, response?.data?.results[0]);
      return response?.data?.results[0];
    } else {
      return {};
    }
  }
};
export const fetchUniqueTournamentDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getUniqueTournamentDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/unique_tournament/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    if (
      response?.data?.results !== undefined &&
      response?.data?.results?.length === 1
    ) {
      setUniqueTournamentDetails(id, response?.data?.results[0]);
      return response?.data?.results[0];
    } else {
      return {};
    }
  }
};
export const fetchUniqueTournamentList = async (fromDate = null) => {
  const sportUrl = `${THE_SPORT_URL}cricket/unique_tournament/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&time=${fromDate}`;
  const url = `${API_URL}callSportsApi`;
  const response = await axios.post(url, { url: sportUrl });

  if (
    response?.data?.results !== undefined &&
    response?.data?.results.length > 0
  ) {
    return response?.data?.results;
  } else {
    return null;
  }
};
export const fetchTournamentDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getTournamentDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/tournament/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });

    if (
      response?.data?.results !== undefined &&
      response?.data?.results?.length === 1
    ) {
      setTournamentDetail(id, response?.data?.results[0]);
      return response?.data?.results[0];
    } else {
      return {};
    }
  }
};
export const fetchTournamentList = async (fromDate = null) => {
  const sportUrl = `${THE_SPORT_URL}cricket/tournament/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&time=${fromDate}`;
  const url = `${API_URL}callSportsApi`;
  const response = await axios.post(url, { url: sportUrl });

  if (
    response?.data?.results !== undefined &&
    response?.data?.results.length > 0
  ) {
    return response?.data?.results;
  } else {
    return null;
  }
};
export const fetchMatchDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }

  const sportUrl = `${THE_SPORT_URL}cricket/match/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
  const url = `${API_URL}callSportsApi`;
  const response = await axios.post(url, { url: sportUrl });

  if (
    response?.data?.results !== undefined &&
    response?.data?.results?.length === 1
  ) {
    return response?.data?.results[0];
  } else {
    return {};
  }
};
export const fetchPlayerDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getPlayerDetail(id);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/player/list?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    if (
      response?.data?.results !== undefined &&
      response?.data?.results?.length === 1
    ) {
      setPlayerDetail(response?.data?.results[0]);
      return response?.data?.results[0];
    } else {
      return {};
    }
  }
};
export const fetchMatchLineUpDetails = async (id = null) => {
  if (id === null || id === "" || id === undefined) {
    return {};
  }
  const cacheResult = getMatchLineUpDetail(id);

  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/match/lineup/detail?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${id}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });

    if (
      response?.data?.results !== undefined &&
      Object.keys(response?.data?.results).length > 0
    ) {
      setMatchLineUpDetail(response?.data?.results);
      return response?.data?.results;
    } else {
      return {};
    }
  }
};
export const fetchMatchListDetails = async (fromDate = null) => {
  const today = Math.round(new Date().getTime() / 1000);
  if (fromDate === null) {
    fromDate = today - 86400;
  }
  const sportUrl = `${THE_SPORT_URL}cricket/match/diary?user=${SPORT_USER}&secret=${SPORT_SECRET}&tsp=${fromDate}`;
  const url = `${API_URL}callSportsApi`;
  const response = await axios.post(url, { url: sportUrl });
  return response?.data?.results !== undefined ? response?.data?.results : {};
};
export const fetchMatchListDetailsWithExtra = async (fromDate = null) => {
  const today = Math.round(new Date().getTime() / 1000);
  if (fromDate === null) {
    fromDate = today - 86400;
  }
  const cacheResult = getMatchLineUpDetail(fromDate);
  if (cacheResult) {
    return cacheResult;
  } else {
    const sportUrl = `${THE_SPORT_URL}cricket/match/diary?user=${SPORT_USER}&secret=${SPORT_SECRET}&tsp=${fromDate}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    return response?.data !== undefined ? response?.data : null;
  }
};
export const fetchLiveMatchListDetails = async () => {
  try {
    const sportUrl = `${THE_SPORT_URL}cricket/match/detail_live?user=${SPORT_USER}&secret=${SPORT_SECRET}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    return response?.data?.results !== undefined
      ? response?.data?.results
      : null;
  } catch (err) {
  } finally {
  }
};
export const fetchHistoricalMatchDetails = async (matchId = null) => {
  if (matchId === null) {
    return null;
  }
  const cacheResult = getHistoricalMatchDetail(matchId);
  if (cacheResult) {
    return cacheResult;
  } else {
    try {
      const sportUrl = `${THE_SPORT_URL}cricket/match/live/history?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${matchId}`;
      const url = `${API_URL}callSportsApi`;
      const response = await axios.post(url, { url: sportUrl });

      if (response?.data?.results !== undefined) {
        setHistoricalMatchDetail(matchId, response?.data?.results);
        return response?.data?.results;
      } else {
        return null;
      }
    } catch (err) {
    } finally {
    }
  }
};
export const fetchLiveOddsList = async () => {
  try {
    const sportUrl = `${THE_SPORT_URL}cricket/odds/live?user=${SPORT_USER}&secret=${SPORT_SECRET}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    console.log("response ===========> ", response);
    return response?.data?.results !== undefined
      ? response?.data?.results
      : null;
  } catch (err) {
  } finally {
  }
};
export const fetchLiveOddsHistory = async (matchId) => {
  if (matchId === null) {
    return null;
  }
  try {
    const sportUrl = `${THE_SPORT_URL}cricket/odds/history?user=${SPORT_USER}&secret=${SPORT_SECRET}&uuid=${matchId}`;
    const url = `${API_URL}callSportsApi`;
    const response = await axios.post(url, { url: sportUrl });
    return response?.data?.results !== undefined
      ? response?.data?.results?.[2]
      : null;
  } catch (err) {
  } finally {
  }
};
