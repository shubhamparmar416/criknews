let venueDetail = {};
let seasonDetail = {};
let teamDetail = {};
let uniqueTournamentDetail = {};
let tournamentDetail = {};
let matchDetail = {};
let playerDetail = {};
let matchLineUpDetail = {};
let historicalMatchDetail = {};
let matchListDetailsWithExtra = {};

export const getVenueDetail = (id) => {
  return !!venueDetail[id] && venueDetail[id] ? venueDetail[id] : null;
};
export const setVenueDetail = (id, data) => {
  venueDetail = {
    ...venueDetail,
    [id]: data,
  };
};

export const getSeasonDetail = (id) => {
  return !!seasonDetail[id] && seasonDetail[id] ? seasonDetail[id] : null;
};
export const setSeasonDetail = (id, data) => {
  venueDetail = {
    ...venueDetail,
    [id]: data,
  };
};

export const getTeamDetail = (id) => {
  return !!teamDetail[id] && teamDetail[id] ? teamDetail[id] : null;
};
export const setTeamDetail = (id, data) => {
  teamDetail = {
    ...teamDetail,
    [id]: data,
  };
};

export const getUniqueTournamentDetail = (id) => {
  return !!uniqueTournamentDetail[id] && uniqueTournamentDetail[id]
    ? uniqueTournamentDetail[id]
    : null;
};
export const setUniqueTournamentDetails = (id, data) => {
  uniqueTournamentDetail = {
    ...uniqueTournamentDetail,
    [id]: data,
  };
};

export const getTournamentDetail = (id) => {
  return !!tournamentDetail[id] && tournamentDetail[id]
    ? tournamentDetail[id]
    : null;
};
export const setTournamentDetail = (id, data) => {
  tournamentDetail = {
    ...tournamentDetail,
    [id]: data,
  };
};

export const getMatchDetail = (id) => {
  return !!matchDetail[id] && matchDetail[id] ? matchDetail[id] : null;
};
export const setMatchDetail = (id, data) => {
  matchDetail = {
    ...matchDetail,
    [id]: data,
  };
};

export const getPlayerDetail = (id) => {
  return !!playerDetail[id] && playerDetail[id] ? playerDetail[id] : null;
};
export const setPlayerDetail = (id, data) => {
  playerDetail = {
    ...playerDetail,
    [id]: data,
  };
};

export const getMatchLineUpDetail = (id) => {
  return !!matchLineUpDetail[id] && matchLineUpDetail[id]
    ? matchLineUpDetail[id]
    : null;
};
export const setMatchLineUpDetail = (id, data) => {
  matchLineUpDetail = {
    ...matchLineUpDetail,
    [id]: data,
  };
};

export const getHistoricalMatchDetail = (id) => {
  return !!historicalMatchDetail[id] && historicalMatchDetail[id]
    ? historicalMatchDetail[id]
    : null;
};
export const setHistoricalMatchDetail = (id, data) => {
  historicalMatchDetail = {
    ...historicalMatchDetail,
    [id]: data,
  };
};

export const getMatchListDetailsWithExtra = (id) => {
  return !!matchListDetailsWithExtra[id] && matchListDetailsWithExtra[id]
    ? matchListDetailsWithExtra[id]
    : null;
};
export const setMatchListDetailsWithExtra = (id, data) => {
  matchListDetailsWithExtra = {
    ...matchListDetailsWithExtra,
    [id]: data,
  };
};
