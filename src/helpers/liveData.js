// create match score data:

import {
  fetchLiveOddsList,
  fetchMatchDetails,
  fetchMatchLineUpDetails,
  fetchMatchListDetails,
  fetchPlayerDetails,
  fetchSeasonDetails,
  fetchTeamDetails,
  fetchTournamentDetails,
  fetchUniqueTournamentDetails,
  fetchVenueDetails,
} from "./utils";

/*
 *Team A alias of Home Team
 *Team B alias of Away Team
 */
export const createLiveMatchDataFromResult = async (list) => {
  let data = [];
  const matchListDetail = await fetchMatchListDetails();
  //const oddsList = await fetchLiveOddsList();

  if (list.length > 0) {
    try {
      await Promise.all(
        list.map(async (items) => {
          const matchDetail = matchListDetail.find((x) => x.id === items.id);
          if (matchDetail !== undefined) {
            const oddsDatakey = await fetchLiveOddsList(items?.id);
            // const lineUpDetail = await fetchMatchLineUpDetails(matchDetail?.id);
            // const firstTeamPlayerLineUp = await getPlayerDetailList(
            //   lineUpDetail,
            //   "firstTeam"
            // );
            // const secondTeamPlayerLineUp = await getPlayerDetailList(
            //   lineUpDetail,
            //   "secondTeam"
            // );

            const oddsData = oddsDatakey?.eu ?? oddsDatakey?.asia ?? null;

            const tempData = {
              matchId: items?.id,
              coverage: matchDetail?.coverage ?? null,
              description: matchDetail?.description ?? null,
              statusId: matchDetail?.status_id ?? items?.score[2],
              seasonId: matchDetail?.season_id ?? null,
              seasonDetails: null,
              venueDetail: await fetchVenueDetails(
                matchDetail?.venue_id ?? null
              ),
              tournamentId: matchDetail?.tournament_id ?? null,
              tournamentDetails: await fetchTournamentDetails(
                matchDetail?.tournament_id ?? null
              ),
              uniqueTournamentId: matchDetail?.unique_tournament_id ?? null,
              matchTime: matchDetail?.match_time ?? null,
              uniqueTournamentDetails: await fetchUniqueTournamentDetails(
                matchDetail?.unique_tournament_id ?? null
              ),
              winner: getWinner(matchDetail?.extra_scores),
              currentRunRate: 0,
              requiredRunRate: 0,
              firstTeam: {
                id: matchDetail?.home_team_id ?? null,
                detail: await fetchTeamDetails(
                  matchDetail?.home_team_id ?? null
                ),
                score: getScore({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.home_team_id ?? null,
                  index: 1,
                }),
                over: getOvers({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.home_team_id ?? null,
                  index: 1,
                }),
                wicket: getWickets({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.home_team_id ?? null,
                  index: 1,
                }),
                player: null,
                batting: getBattingTeam(items?.score, 1),
                oddsData: oddsData?.[1] ?? 0,
              },
              secondTeam: {
                id: matchDetail?.away_team_id ?? null,
                detail: await fetchTeamDetails(
                  matchDetail?.away_team_id ?? null
                ),
                score: getScore({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.away_team_id ?? null,
                  index: 2,
                }),
                over: getOvers({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.away_team_id ?? null,
                  index: 2,
                }),
                wicket: getWickets({
                  score: items?.score ?? [],
                  timeline: items?.timeline ?? [],
                  teamId: matchDetail?.away_team_id ?? null,
                  index: 2,
                }),
                player: null,
                batting: getBattingTeam(items?.score, 2),
                oddsData: oddsData?.[3] ?? 0,
              },
            };
            tempData.currentRunRate = getRunRate(
              tempData.firstTeam,
              tempData.secondTeam,
              "CURRENT"
            );
            tempData.requiredRunRate = getRunRate(
              tempData.firstTeam,
              tempData.secondTeam,
              "CURRENT"
            );
            tempData.currentSituation = getCurrentSituation(
              tempData,
              items?.timeline.length
            );

            data.push(tempData);
          }
        })
      );
    } catch (e) {
      console.log("error ======", e);
    } finally {
      return data;
    }
  }
};
export const synchLiveVarriableMatchDataFromResult = async (
  existingList,
  updatedList
) => {
  if (existingList) {
    existingList.map((items) => {
      return items.map((item) => {
        const updatedRow = updatedList.find((x, index) => {
          return x.id === item.matchId;
        });

        if (updatedRow) {
          item.firstTeam.score = getScore({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 1,
          });
          item.firstTeam.over = getOvers({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 1,
          });
          item.firstTeam.wicket = getWickets({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 1,
          });
          item.secondTeam.score = getScore({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 2,
          });

          item.secondTeam.over = getOvers({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 2,
          });
          item.secondTeam.wicket = getWickets({
            score: updatedRow?.score ?? [],
            timeline: updatedRow?.timeline ?? [],
            teamId: item.id ?? null,
            index: 2,
          });
          item.currentRunRate = getRunRate(
            item.firstTeam,
            item.secondTeam,
            "CURRENT"
          );
          item.currentSituation = getCurrentSituation(
            item,
            items?.timeline.length
          );
        }

        return item;
      });
    });
  }

  return existingList;
};

export const synchSingleMatchScoreDetail = async (oldResult, newResult) => {
  const tempData = { ...oldResult };

  tempData.firstTeam.score = getScore({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.firstTeam?.id ?? null,
    index: 1,
  });
  tempData.firstTeam.over = getOvers({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.firstTeam?.id ?? null,
    index: 1,
  });
  tempData.firstTeam.wicket = getWickets({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.firstTeam?.id ?? null,
    index: 1,
  });
  tempData.secondTeam.score = getScore({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.secondTeam?.id ?? null,
    index: 2,
  });

  tempData.secondTeam.over = getOvers({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.secondTeam?.id ?? null,
    index: 2,
  });
  tempData.secondTeam.wicket = getWickets({
    score: newResult?.score ?? [],
    timeline: newResult?.timeline ?? [],
    teamId: tempData?.secondTeam?.id ?? null,
    index: 2,
  });
  tempData.currentRunRate = getRunRate(
    tempData.firstTeam,
    tempData.secondTeam,
    "CURRENT"
  );

  return tempData;
};
export const synchSingleMatchExtraDetail = async () => {
  const tempData = {};

  return tempData;
};
export const createLiveMatchDataForLivePage = async (list) => {
  let data = [];

  if (list.length > 0) {
    await Promise.all(
      list.map(async (items) => {
        const matchDetail = await fetchMatchDetails(items.id);
        const lineUpDetail = await fetchMatchLineUpDetails(matchDetail?.id);
        const firstTeamPlayer = await getPlayerDetailList(
          lineUpDetail,
          "firstTeam"
        );
        const secondTeamPlayer = await getPlayerDetailList(
          lineUpDetail,
          "secondTeam"
        );
        if (matchDetail) {
          const tempData = {
            matchId: matchDetail?.id,
            coverage: matchDetail?.coverage,
            description: matchDetail?.description,
            statusId: matchDetail?.status_id,
            seasonId: matchDetail?.season_id,
            seasonDetails: await fetchSeasonDetails(matchDetail?.season_id),
            venueId: matchDetail?.venue_id,
            venueDetail: await fetchVenueDetails(matchDetail?.venue_id),
            tournamentId: matchDetail?.tournament_id,
            tournamentDetails: await fetchTournamentDetails(
              matchDetail?.tournament_id
            ),
            uniqueTournamentId: matchDetail?.unique_tournament_id,
            matchTime: matchDetail?.match_time,
            uniqueTournamentDetails: await fetchUniqueTournamentDetails(
              matchDetail?.unique_tournament_id
            ),
            winner: getWinner(matchDetail?.extra_scores),
            lineUpDetail: fetchMatchLineUpDetails(matchDetail?.id),
            firstTeam: {
              id: matchDetail?.home_team_id ?? null,
              detail: await fetchTeamDetails(matchDetail?.home_team_id),
              score: getScore(items?.score, 0),
              over: getOvers(matchDetail?.extra_scores, 0),
              wicket: getWickets(matchDetail?.extra_scores, 0),
              player: firstTeamPlayer,
            },
            secondTeam: {
              id: matchDetail?.away_team_id ?? null,
              detail: await fetchTeamDetails(matchDetail?.away_team_id),
              score: getScore(items?.score, 1),
              over: getOvers(matchDetail?.extra_scores, 1),
              wicket: getWickets(matchDetail?.extra_scores, 1),
              player: secondTeamPlayer,
            },
            TotalOver: 20,
          };
          data.push(tempData);
        }
      })
    );
  }

  return data;
};

const getScore = ({ score = [], timeline = [], teamId = null, index }) => {
  if (
    score.length === 0 ||
    timeline.length === 0 ||
    (score.length === 1 && index === 2)
  ) {
    return 0;
  }
  const isEven = timeline.length % 2 === 0;
  if (isEven) {
    const lastKey = timeline.length - 1;
    const secondLastKey = lastKey - 1;

    return score[4]?.innings[lastKey] !== undefined
      ? score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][1]
        : score[4]?.innings[secondLastKey][1]
      : 0;
  } else {
    const lastKey = timeline.length - 1;
    if (score[4]?.innings[lastKey] !== undefined) {
      return score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][1]
        : 0;
    } else {
      return 0;
    }
  }
};

const getOvers = ({ score = [], timeline = [], teamId = null, index }) => {
  if (
    score.length === 0 ||
    timeline.length === 0 ||
    (score.length === 1 && index === 2)
  ) {
    return 0;
  }
  const isEven = timeline.length % 2 === 0;
  if (isEven) {
    const lastKey = timeline.length - 1;
    const secondLastKey = lastKey - 1;
    if (score[4]?.innings[lastKey] !== undefined) {
      return score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][2]
        : score[4]?.innings[secondLastKey][2];
    }
    return 0;
  } else {
    const lastKey = timeline.length - 1;
    if (score[4]?.innings[lastKey] !== undefined) {
      return score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][2]
        : 0;
    }
    return 0;
  }
};
const getWickets = ({ score = [], timeline = [], teamId = null, index }) => {
  if (
    score.length === 0 ||
    timeline.length === 0 ||
    (score.length === 1 && index === 2)
  ) {
    return 0;
  }
  const isEven = timeline.length % 2 === 0;
  if (isEven) {
    const lastKey = timeline.length - 1;
    const secondLastKey = lastKey - 1;
    if (score[4]?.innings[lastKey] !== undefined) {
      return score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][3]
        : score[4]?.innings[secondLastKey][3];
    }
    return 0;
  } else {
    const lastKey = timeline.length - 1;
    if (score[4]?.innings[lastKey] !== undefined) {
      return score[4]?.innings[lastKey][0] === index
        ? score[4]?.innings[lastKey][3]
        : 0;
    }
    return 0;
  }
};
const getBattingTeam = (score, teamIndex) => {
  if (score === undefined || score.length === 0 || score[2] === undefined) {
    return false;
  }
  return score[2] === teamIndex;
};

const getWinner = (extra_scores, index) => {
  if (extra_scores === undefined || extra_scores?.results === null) {
    return "No Result";
  }
  if (extra_scores?.results?.result === 1) {
    return "firstTeam";
  } else if (extra_scores?.results?.result === 2) {
    return "secondTeam";
  } else {
    return "Draw";
  }
};
const getPlayerDetailList = async (data, team) => {
  if (
    data?.lineup === undefined ||
    team === undefined ||
    data?.lineup === null ||
    team === null
  ) {
    return null;
  }
  const key = team === "firstTeam" ? "home" : "away";
  const list = data.lineup[key];
  const tempData = [];
  if (list.length > 0) {
    await Promise.all(
      list.map(async (item) => {
        const playerDetails = await fetchPlayerDetails(item?.id);
        item = {
          ...item,
          name: playerDetails.name,
          shortName: playerDetails.shortName,
          logo: playerDetails.logo,
        };
        tempData.push(item);
      })
    );
  }
  return tempData;
};
const getRunRate = (firstTeam, secondTeam, type = "CURRENT") => {
  switch (type) {
    case "CURRENT":
      if (firstTeam.score === 0) {
        return 0;
      }
      let run = firstTeam.score;
      if (typeof firstTeam.over == "number" && !isNaN(firstTeam.over)) {
        return run / firstTeam.over;
      } else {
        return 0;
      }

    case "REQUIRED":
      let tempRun = secondTeam.score;
      if (typeof firstTeam.over == "number" && !isNaN(firstTeam.over)) {
        return tempRun / firstTeam.over;
      } else {
        return 0;
      }

    default:
      return 0;
  }
};
const getCurrentSituation = (matchDetail, inning) => {
  if (inning === 0) {
    return null;
  }

  if (matchDetail.firstTeam.batting && inning % 2 === 0) {
    return `${
      matchDetail.secondTeam.score - (matchDetail.firstTeam.score + 1)
    } Required to win `;
  } else if (matchDetail.secondTeam.batting && inning % 2 === 0) {
    return `${
      matchDetail.firstTeam.score - (matchDetail.secondTeam.score + 1)
    } Required to win `;
  } else {
    return null;
  }
};
const getOddsdata = (id, teamIndex, list = []) => {
  if (list.length > 0) {
    const keys = list?.["2"];
    console.log("keys ======> ", keys);
  }
  return 0;
};
