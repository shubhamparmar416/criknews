import {
  fetchTournamentDetails,
  fetchVenueDetails,
  isEmpty,
  isUndefined,
} from "./utils";
import { MATCH_TYPE } from "../constant";

export const createUpcomingMatchDataForPage = async (list, results_extra) => {
  let data = [];
  try {
    const { team, unique_tournament } = results_extra;

    if (list.length > 0) {
      await Promise.all(
        list.map(async (items) => {
          if (items.status_id === 1) {
            const { type } = await fetchTournamentDetails(items?.tournament_id);
            const uniqueTournamentDetail =
              results_extra?.unique_tournament?.find(
                (x) => x.id === items.unique_tournament_id
              );
            if (type !== 0) {
              const tempData = {
                statusId: items?.status_id,
                matchId: items?.id,
                venueId: items.venue_id,
                venueDetail: await fetchVenueDetails(items.venue_id),
                tournamentId: items.tournament_id,
                matchTitle: getTournamentTitle(
                  unique_tournament,
                  items?.unique_tournament_id
                ),
                matchType: MATCH_TYPE[type],
                matchTime: items.match_time,
                firstTeam: {
                  id: items.home_team_id ?? null,
                  detail: await getTeamDetails(team, items.home_team_id),
                },
                secondTeam: {
                  id: items.away_team_id ?? null,
                  detail: await getTeamDetails(team, items.away_team_id),
                },
                uniqueTournamentDetail,
              };

              data.push(tempData);
            }
          }
        })
      );
    }
  } catch (error) {
    console.log("error ========== > ", error);
  }

  return data;
};

const getTeamDetails = (team, id) => {
  if (isUndefined(id) || isEmpty(id)) {
    return null;
  }
  const tempObj = team.find((x) => x.id === id);
  if (!tempObj) {
    return null;
  }
  return tempObj;
};
const getTournamentTitle = (unique_tournament, id) => {
  if (isUndefined(id) || isEmpty(id)) {
    return null;
  }
  const tempObj = unique_tournament.find((x) => x.id === id);
  if (isUndefined(tempObj?.name) || isEmpty(tempObj?.name)) {
    return null;
  }
  return tempObj?.name;
};
