import {
  fetchTournamentDetails,
  fetchVenueDetails,
  isEmpty,
  isUndefined,
} from "./utils";
import { MATCH_TYPE } from "../constant";

/*
 * create Recent match data
 *First Team alias of Home Team
 *Second Team alias of Away Team
 */
export const createRecentMatchDataForCarousal = async (list, results_extra) => {
  let data = [];
  const { team, unique_tournament } = results_extra;
  if (list.length > 0) {
    await Promise.all(
      list.map(async (items) => {
        if (items.status_id === 100) {
          const tempData = {
            statusId: items?.status_id,
            matchId: items?.id,
            venueId: items.venue_id,
            venueDetail: await fetchVenueDetails(items.venue_id),
            tournamentId: items.tournament_id,
            matchTitle: getTournamentTitle(
              unique_tournament,
              items?.unique_tournament_id
            ),
            matchTime: items.match_time,
            firstTeam: {
              id: items.home_team_id ?? null,
              // detail: await fetchTeamDetails(items.home_team_id),
              detail: await getTeamDetails(team, items.home_team_id),
              score: getScore(items.scores, 0),
              over: getOvers(items.extra_scores, 0),
              wicket: getWickets(items.extra_scores, 0),
            },
            secondTeam: {
              id: items.away_team_id ?? null,
              // detail: await fetchTeamDetails(items.away_team_id),
              detail: await getTeamDetails(team, items.away_team_id),
              score: getScore(items.scores, 1),
              over: getOvers(items.extra_scores, 1),
              wicket: getWickets(items.extra_scores, 1),
            },
          };
          data.push(tempData);
        }
      })
    );
  }

  return data;
};

export const createRecentMatchDataForPage = async (list, results_extra) => {
  let data = [];
  const { team, unique_tournament } = results_extra;
  if (list.length > 0) {
    await Promise.all(
      list.map(async (items) => {
        if (items.status_id === 100) {
          const { type } = await fetchTournamentDetails(items?.tournament_id);
          if (type !== 0) {
            const tempData = {
              statusId: items?.status_id,
              matchId: items?.id,
              venueId: items.venue_id,
              venueDetail: await fetchVenueDetails(items.venue_id),
              tournamentId: items.tournament_id,
              matchTitle: getTournamentTitle(
                unique_tournament,
                items?.unique_tournament_id
              ),
              matchType: MATCH_TYPE[type],
              matchTime: items.match_time,
              firstTeam: {
                id: items.home_team_id ?? null,
                // detail: await fetchTeamDetails(items.home_team_id),
                detail: await getTeamDetails(team, items.home_team_id),
                score: getScore(items.scores, 0),
                over: getOvers(items.extra_scores, 0),
                wicket: getWickets(items.extra_scores, 0),
              },
              secondTeam: {
                id: items.away_team_id ?? null,
                // detail: await fetchTeamDetails(items.away_team_id),
                detail: await getTeamDetails(team, items.away_team_id),
                score: getScore(items.scores, 1),
                over: getOvers(items.extra_scores, 1),
                wicket: getWickets(items.extra_scores, 1),
              },
            };
            tempData.matchResult = getMatchResult(
              items?.extra_scores,
              tempData
            );
            data.push(tempData);
          }
        }
      })
    );
  }

  return data;
};
const getScore = (scores, index) => {
  if (scores === undefined) {
    return 0;
  }
  const length = Object.keys(scores).length;
  if (length === 0) {
    return 0;
  }
  const key = `p${length - 1}`;
  return scores[key] === undefined ? 0 : scores[key][index];
};
const getOvers = (extra_scores, index) => {
  if (
    extra_scores === undefined ||
    extra_scores?.innings === undefined ||
    extra_scores?.innings[index] === undefined
  ) {
    return 0;
  }
  return extra_scores?.innings[index][2] === undefined
    ? 0
    : extra_scores?.innings[index][2];
};
const getWickets = (extra_scores, index) => {
  if (
    extra_scores === undefined ||
    extra_scores?.innings === undefined ||
    extra_scores?.innings[index] === undefined
  ) {
    return 0;
  }
  return extra_scores?.innings[index][3] === undefined
    ? 0
    : extra_scores?.innings[index][3];
};
const getTournamentTitle = (unique_tournament, id) => {
  if (isUndefined(id) || isEmpty(id)) {
    return null;
  }
  const tempObj = unique_tournament.find((x) => x.id === id);
  if (isUndefined(tempObj?.name) || isEmpty(tempObj?.name)) {
    return null;
  }
  return tempObj?.name;
};
const getTeamDetails = (team, id) => {
  if (isUndefined(id) || isEmpty(id)) {
    return null;
  }
  const tempObj = team.find((x) => x.id === id);
  if (!tempObj) {
    return null;
  }
  return tempObj;
};
const getMatchResult = (extra_scores, match) => {
  if (Object.keys(extra_scores).length === 0) {
    return null;
  }
  const { results } = extra_scores;
  if (!results) {
    return null;
  }
  let statement = "";

  switch (results?.result) {
    case 1:
      statement += `${
        isEmpty(match?.firstTeam?.detail?.short_name)
          ? match?.firstTeam?.detail?.name
          : match?.firstTeam?.detail?.short_name
      } won by ${results?.margin}`;
      break;
    case 2:
      statement += `${
        isEmpty(match?.secondTeam?.detail?.short_name)
          ? match?.secondTeam?.detail?.name
          : match?.secondTeam?.detail?.short_name
      } won by ${results?.margin}`;
      break;
    case 3:
      statement = "Draw";
      break;

    default:
      break;
  }
  if (statement) {
    switch (results?.winby) {
      case 1:
        statement += " runs ";
        break;
      case 2:
        statement += " wickets ";
        break;

      default:
        statement = null;
        break;
    }
  }

  return statement;
};
