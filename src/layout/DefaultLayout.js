import React, { useState } from "react";
import AppContent from "../components/generic/AppContent";
import AppFooter from "../components/generic/AppFooter";
import AppHeader from "../components/generic/AppHeader";
import AppNavBar from "../components/generic/AppNavBar";
import AppSidebar from "../components/generic/AppSideBar";

const DefaultLayout = () => {
  const [showSideBar, setShowSideBar] = useState(false);
  return (
    <div>
      <AppHeader />
      <AppNavBar />
      {showSideBar && <AppSidebar />}
      <AppContent setShowSideBar={setShowSideBar} />
      <AppFooter />
    </div>
  );
};

export default DefaultLayout;
